

angular.module('http-3rd', [])
    /**
     * 拦截http请求
     */
    .factory('httpInterceptor', ["$q", '$filter', '$rootScope', function ($q, $filter, $rootScope) {
        return {
            //请求开始
            'request': function (config) {
                return config;
            },
            //响应
            'response': function (response) {
                var data = response.data;
                if (data && angular.isObject(data)) {
                    if (data.status && data.status != 0) {
                        $rootScope.Notify.alert({
                           message: data.msg,
                           hideCancel: true
                        });
                        return $q.reject(response);

                    }
                }
                return response;
            },

            //响应失败
            'responseError': function (rejection) {

                $rootScope.Notify.alert({
                   message: "网络错误",
                   hideCancel: true
                });
                return $q.reject();
            },

            //请求失败
            'requestError': function (rejection) {
                $rootScope.Notify.alert({
                    message: "客户端请求出错",
                    hideCancel: true
                });
                return $q.reject(rejection);
            }
        }
    }])
    .config(['$httpProvider', function ($httpProvider) {

        /**
         * angular 默认的 http post请求设置需要改写, 不符合常用的要求
         * 需要修改data以及 Content-Type
         * */

            //更改默认Content-Type
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        //添加HTTP头部，说明是ajax请求(like jQuery)
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


        /**
         *
         *  param 方法
         *  用于format post请求的data， angular默认的是 {a:1, b:1}
         *  format 后为a=1&b=1
         *
         * */
        var param = function (obj) {
            var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

            for (name in obj) {
                value = obj[name];

                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value !== undefined && value !== null)
                    query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
            }

            return query.length ? query.substr(0, query.length - 1) : query;
        };


        // Override $http service's default transformRequest
        $httpProvider.defaults.transformRequest = [function (data) {
            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];

        $httpProvider.interceptors.push('httpInterceptor');
    }]);