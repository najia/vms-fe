/**
 * Created by Jessie on 2017/4/29.
 */


var APP_GLOBAL = {
    getNowFormatDate: function () {
        var date = new Date();
        var sep1 = '-';
        var sep2 = ':';
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        var strHours = date.getHours();
        var strMinutes = date.getMinutes();
        if (month >= 1 && month <= 9) {
            month = '0' + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = '0' + strDate;
        }
        if (strHours >= 0 && strHours <= 9) {
            strHours = '0' + strHours;
        }

        if (strMinutes >= 0 && strMinutes <= 9) {
            strMinutes = '0' + strMinutes;
        }
        var currentDate = date.getFullYear() + sep1 + month + sep2 + strDate
            + " " + strHours + sep2 + strMinutes;
        return currentDate;
    },
    preview: function () {
        $('#print-area').jqprint();
        // var el = document.getElementById('print-area');
        // var iframe = document.createElement('IFRAME');
        // var doc = null;
        // iframe.setAttribute('style', 'position:absolute;width:0px;height:0px;left:-500px;top:-500px;');
        // document.body.appendChild(iframe);
        // doc = iframe.contentWindow.document;
        // doc.write('<div>' + el.innerHTML + '</div>');
        // doc.close();
        // iframe.contentWindow.focus();
        // iframe.contentWindow.print();
        // if (navigator.userAgent.indexOf("MSIE") > 0) {
        //     document.body.removeChild(iframe);
        // }
    }
};

window.APP_GLOBAL = APP_GLOBAL;

function onlyNumber(obj) {
    // 得到第一个字符是否为负号
    // var t = obj.value.charAt(0);
    // 先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d\.]/g, '');
    // 必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g, '');
    // 保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g, '.');
    // 保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.');
    // 如果第一位是负号，则允许添加
    //if (t == '-') {
    //    obj.value = '-' + obj.value;
    //}
}

function onlyNumber2(obj) {
    if (obj.value.length == 1) {
        obj.value = obj.value.replace(/[^1-9]/g, '')
    } else {
        obj.value = obj.value.replace(/\D/g, '')
    }
}

window.onlyNumber = onlyNumber;
window.onlyNumber2 = onlyNumber2;