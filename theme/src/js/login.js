/**
 * Created by Jessie on 2017/4/25.
 */


function init() {
    var loginForm = $('#form-sign');
    if (!loginForm.length) return;

    loginForm.validate({
        rules: {
            'username': {required: true},
            'password': {required: true},
            'captcha': {required: true}
        },
        errorPlacement: function(error, element){
            error.appendTo(element.parent());
        },
        messages: {
            'username': {
                required: '请输入用户名'
            },
            'password': {
                required: '请输入密码'
            },
            'captcha': {
                required: '请输入验证码'
            }
        },
        submitHandler:function(form)
        {
            $('#login-alert').hide();
            $.post('/admin/login', loginForm.serialize(), function (data) {
                if (data.status == 0) {
                    $('#login-alert').hide();
                    window.location.href = '/gateway';
                } else {
                    $('#login-alert').show().text(data.msg)
                }
            }, 'json');
        }

    });
}


init();