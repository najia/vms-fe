/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./labor.html');
var employeeModal = require('./employeeModal.html');
angular
    .module('app.labor', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/labor', {
                templateUrl: pageUrl,
                controller: 'laborController',
                state: 'labor'
            })
        ;
    }])
    // 主活动详情
    .controller('laborController', ['$scope', '$routeParams', 'laborIF', 'Notify', 'employeeModal', 'Alert', function($scope, $routeParams, laborIF, Notify, employeeModal, Alert) {
        var form = $scope.form = {};
        $scope.list = [];
        $scope.isUpdate = false;
        var pageInfo = $scope.pageInfo = [];
        var laborType = $scope.laborType = [{
            labor_type: 1,
            name: '日常养护'
        }, {
            labor_type: 2,
            name: '专项养护'
        }, {
            labor_type: 3,
            name: '保洁'
        }];

        var categoryJson = $scope.categoryJson = [];

        function list() {
            laborIF.listLabor().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.list = data.data;
                }
            });
        }

        $scope.pageChanged = function() {
            list();
        };
        $scope.search = function() {
            list();
        };

        $scope.reset = function() {
            form.username = '';
            form.offset = 1;
            list();
        };

        $scope.save = function() {
            var newObj = angular.extend({}, form);
            var total = 0;
            console.log(categoryJson);
            categoryJson.map(function (v) {
                delete v.$$hashKey;
                delete v.id;
                delete v.organization_id;
                delete v.status;
                total += parseInt(v.labor_employee) || 0;
            });
            newObj.categoryJson = JSON.stringify(categoryJson);
            newObj.start_no = newObj.start_no.start_no;
            newObj.end_no = newObj.end_no.end_no;
            newObj.num_employee = total;

            if ($scope.isUpdate) {
                console.log(newObj);
                delete newObj.organization_id;
                delete newObj.status;
                laborIF.update(newObj).success(function(data) {
                    if (data.status == 0) {
                        list();
                        form = $scope.form = {};
                        categoryJson = $scope.categoryJson = [];
                    }
                });
            } else {
                console.log(newObj);
                laborIF.save(newObj).success(function(data) {
                    if (data.status == 0) {
                        list();
                        form = $scope.form = {};
                        categoryJson = $scope.categoryJson = [];
                    }
                });
            }
        };

        function getCategory(data) {
            var result = [];
            for (var key in data) {
                for (var key2 in laborType) {
                    if (data[key].labor_type == laborType[key2].labor_type) {
                        result.push(laborType[key2]);
                    }
                }
            }
            return result;
        }
        $scope.removeCategory = function (item, model) {
            $scope.categoryJson.map(function (v, k) {
                 if (v.labor_type == item.labor_type) {
                     $scope.categoryJson.splice(k, 1);
                 }
            });
        };
        $scope.checkCategory = function (item) {
            $scope.categoryJson.push({
                labor_type: item.labor_type,
                labor_currency: '',
                labor_content: '',
                labor_employee: ''
            });
        };
        $scope.toEdit = function(item) {
            $scope.isUpdate = true;
            form.road_name = item.road_name;
            $scope.changeRoad();
            form = $scope.form = angular.extend({}, item);
            form.start_no = {
                start_no: form.start_no
            };
            form.end_no = {
                end_no: form.end_no
            };
            categoryJson = $scope.categoryJson = form.categoryJson;
            form.categoryJson = getCategory(form.categoryJson);
            console.log(form);

        };
        $scope.cancelEdit = function() {
            $scope.isUpdate = false;
            form = $scope.form = {};
            categoryJson = $scope.categoryJson = [];
        };
        $scope.delete = function(id) {
            var obj = {
                labor_id: id
            };
            Notify.alert('确定删除?').result.then(function () {
                laborIF.delete(obj).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        list();
                    }
                });
            });

        };
        $scope.openRight = function(item) {
            var obj = {
                user_id: item.user_id
            };
            if (item.status == 0) {
                laborIF.closeRight(obj).success(function(data) {
                    if (data.status == 0) {
                        item.status = 1;
                    }
                });
            } else {
                laborIF.openRight(obj).success(function(data) {
                    if (data.status == 0) {
                        item.status = 0;
                    }
                });
            }
        };

        // 选择时间
        $scope.status = {};
        $scope.startDateOptions = {
            change: function () {
                $scope.endDateOptions.minDate = $scope.form.start_time;
            }
        };
        $scope.endDateOptions = {
            change: function () {
                $scope.startDateOptions.maxDate = new Date($scope.form.end_time);
            }
        };
        $scope.open = function ($event, type) {
            $scope.status[type + 'Opened'] = true;
            $event.preventDefault();
            $event.stopPropagation();
        };


        function listRoadCodeList() {
            laborIF.listRoadCodeList('labor').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.roadCodeList = data.data;
                }
            });
        }

        function listStartNo() {
            laborIF.listStartNo(form.road_name, 'labor').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.startNo = data.data;
                }
            });
        }

        function listEndNo() {
            laborIF.listEndNo(form.road_name, 'labor').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.endNo = data.data;
                }
            });
        }

        function listLaneNu() {
            laborIF.listLaneNu(form.road_name, 'labor').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.laneNum = data.data;
                }
            });
        }

        $scope.toManageEmployee = function (id) {
            employeeModal.exec(id).result.then(function(msg) {

            });
        };
        $scope.changeRoad = function () {
            form.start_no = '';
            form.end_no = '';
            form.lane_no = '';
            listStartNo();
            listEndNo();
            listLaneNu();
        };

        list();
        listRoadCodeList();
    }])
    .controller('employeeModalController', ['$scope', '$uibModalInstance', 'item', 'laborIF', 'Notify', 'Alert', function ($scope, $uibModalInstance, item, laborIF, Notify, Alert) {
        var form = $scope.form = {};
        var list = $scope.list = {
            data: [],
            form: {
                offset: 1,
                count: 5,
                total: 0,
                labor_id: item,
                username: ''
            }
        };
        $scope.isUpdate = false;


        function init() {
            laborIF.listEmployee(list.form).success(function(data) {
                if (data.status == 0) {
                    list.data = $scope.list.data = data.data.list;
                    $scope.list.form.total = data.data.total;
                }
            });
        }
        $scope.pageChanged = function() {
            init();
        };
        $scope.search = function() {
            init();
        };

        $scope.reset = function() {
            list.form.username = '';
            list.form.offset = 1;
            init();
        };
        $scope.save = function() {
            if ($scope.isUpdate) {
                laborIF.updateEmployee(form).success(function(data) {
                    if (data.status == 0) {
                        init();
                    }
                });
            } else {
                form.labor_id = item;
                laborIF.saveEmployee(form).success(function(data) {
                    if (data.status == 0) {
                        init();
                    }
                });
            }
        };

        $scope.toEdit = function(item) {
            $scope.isUpdate = true;
            form = $scope.form = angular.extend({}, item);

        };
        $scope.cancelEdit = function() {
            $scope.isUpdate = false;
            form = $scope.form = {};
        };

        $scope.delete = function(id) {
            var obj = {
                employee_id: id
            };
            Notify.alert('确定删除?').result.then(function () {
                laborIF.deleteEmployee(obj).success(function(data) {
                    if (data.status == 0) {
                        Alert.alert('删除成功');
                        init();
                    }
                });
            });

        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        init();
    }])
    .service('employeeModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: employeeModal,
                    controller: 'employeeModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    },
                    size: 'lg'
                });
            }
        }
    }])
    .service('laborIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var laborIF = {
            save: function (params) {
                return $http.post('/labor/api/add', params);
            },
            update: function (params) {
                return $http.post('/labor/api/edit', params);
            },
            delete: function (params) {
                return $http.post('/labor/api/delete', params);
            },
            listEmployee: function (params) {
                return $http.get('/labor/employee/getlist', {
                    params: params
                });
            },
            saveEmployee: function (params) {
                return $http.post('/labor/employee/add', params);
            },
            updateEmployee: function (params) {
                return $http.post('/labor/employee/edit', params);
            },
            deleteEmployee: function (params) {
                return $http.post('/labor/employee/delete', params);
            }
        };
        return angular.extend({}, commonData, laborIF);
    }])
;





