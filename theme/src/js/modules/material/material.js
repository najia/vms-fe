/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./material.html');
var addMaterialModal = require('./addMaterialModal.html');
var newMaterial = require('./newMaterial.html');
var confirmDataModal = require('./confirmDataModal.html');

angular
    .module('app.material', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/material', {
                templateUrl: pageUrl,
                controller: 'materialController',
                state: 'material'
            })
        ;
    }])
    .controller('materialController', ['$scope', '$routeParams', 'materialIF', 'materialModal', 'Notify', 'Alert', 'confirmMaterialDataModal', function($scope, $routeParams, materialIF, materialModal, Notify, Alert, confirmMaterialDataModal) {

        $scope.userList = [];
        // 操作用户
        var operateUser = $scope.operateUser = {
            target_user_id: {}
        };
        // 材料
        var material = $scope.material = {
            data: [],
            form: {
                offset: 1,
                count: 5,
                material_name: '',
                target_user_id: '',
                op_type: '-1'
            }
        };
        // 库存预警
        var warningMaterial = $scope.warningMaterial = {
            data: [],
            form: {
                offset: 1,
                count: 5
            }
        };

        // 库存预警
        var operateData = $scope.operateData = {
            data: [],
            form: {
                offset: 1,
                count: 5
            }
        };

        // 临时需要编辑的材料
        var newMaterial = $scope.newMaterial = [];

        // 页面加载的时候判断是否有选中的项
        function checkMaterialLoad() {
            var oldData = material.data || [];
            var newData = newMaterial || [];
            var oldDataItem = null;
            var newDataItem = null;
            for (var i = 0; i < oldData.length; i++) {
                oldDataItem = oldData[i];
                if (newData.length == 0) {
                    oldDataItem.isChecked = false;
                }
                for (var j = 0; j < newData.length; j++) {
                    newDataItem = newData[j];
                    if (oldDataItem.material_id == newDataItem.material_id) {
                        oldDataItem.isChecked = true;
                    }
                }
            }
        }
        // 删除的时候， 把先前选中的项去掉。
        function checkMaterialDelete(id) {
            var oldData = material.data || [];
            var oldDataItem = null;
            for (var i = 0; i < oldData.length; i++) {
                oldDataItem = oldData[i];
                if (oldDataItem.material_id == id) {
                    oldDataItem.isChecked = false;
                }
            }
        }

        function listMaterial() {
            materialIF.listMaterial(material.form).success(function(data) {
                if (data.status == 0 && data.data && data.data.list) {
                    material.data = data.data.list;
                    checkMaterialLoad();
                    material.form.total = data.data.total;
                }
            });
        }

        function listUser() {
            var type = {
                type: 7
            };
            materialIF.listUser(type).success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.userList = data.data;
                }
            });
        }

        function listWarningMaterial() {
            materialIF.listWarningMaterial(warningMaterial.form).success(function(data) {
                if (data.status == 0 && data.data) {
                    warningMaterial.data = data.data.list;
                    warningMaterial.form.total = data.data.total;
                }
            });
        }

        function listOperateData() {
            materialIF.listOperateData(operateData.form).success(function(data) {
                if (data.status == 0 && data.data) {
                    operateData.data = data.data.list;
                    operateData.form.total = data.data.total;
                }
            });
        }

        $scope.toAddMaterial = function () {
            materialModal.exec().result.then(function(msg) {
                Alert.alert(msg);
                listMaterial();
            });
        };

        $scope.searchMaterial = function () {
            material.form.offset = 1;
            listMaterial();
        };

        $scope.checkMaterial = function ($event, item, index) {
            var checkbox = $event.target;
            if (!operateUser.target_user_id.user_id || material.form.op_type == '-1') {
                Alert.alert('请先选择【材料操作人员】和【操作类别】', 'danger');
                checkbox.checked = false;
                return;
            }

            var action = (checkbox.checked ? 'add' : 'remove');

            item.isChecked = true;
            if (action == 'add') {
                var shortMaterial = angular.extend({}, item);
                newMaterial.push(shortMaterial);

            }
        };

        $scope.pageChangedMaterial = function() {
            listMaterial();
        };

        $scope.pageChangedWarningMaterial = function() {
            listWarningMaterial();
        };

        $scope.pageChangedOperateData = function() {
            listOperateData();
        };

        $scope.checkUser = function () {
            material.form.offset = 1;
            material.form.target_user_id = operateUser.target_user_id.user_id;
            listMaterial();
        };

        $scope.checkOperate = function () {
            if (!operateUser.target_user_id.user_id) {
                material.form.op_type = '-1';
                Alert.alert('请先选择【材料操作人员】', 'danger');
                return;
            }
            material.form.offset = 1;
            newMaterial = $scope.newMaterial = [];
            listMaterial();
        };

        $scope.deleteMaterial = function (index, id) {
            if (newMaterial.length == 1) {
                newMaterial = $scope.newMaterial = [];
            } else {
                $scope.newMaterial.splice(index, 1);
            }
            checkMaterialDelete(id);
        };

        function handleSuccess() {
            newMaterial = $scope.newMaterial = [];
            material.form.offset = 1;
            listMaterial();

            operateData.form.offset = 1;
            listOperateData();

            warningMaterial.form.offset = 1;
            listWarningMaterial();

            Alert.alert('操作成功！');

            warningMaterial.form.offset = 1;
            listWarningMaterial();

            operateData.form.offset = 1;
            listOperateData();
        }

        $scope.saveMaterial = function () {
            var isSuccess = true;

            var stringMaterial = newMaterial.map(function (value, index, array) {
                if (!value.num) {
                    isSuccess = false;
                    return false;
                } else {
                    return {
                        id: value.material_id,
                        num: value.num

                    };
                }

            });

            if (!isSuccess || !newMaterial.length || !operateUser.target_user_id.user_id) {
                Alert.alert('请完善信息', 'danger');
                return false;
            }

            var newObj = {
                target_user_id: operateUser.target_user_id.user_id,
                materials: JSON.stringify(stringMaterial)
            };
            console.log(newMaterial);
            var operateText = '采购';
            if (material.form.op_type == 'in') {
                operateText = '入库';
            } else if (material.form.op_type == 'out') {
                operateText = '出库';
            }
            var confirmData = {
                newMaterial: newMaterial,
                operateType: operateText
            };
            confirmMaterialDataModal.exec(confirmData).result.then(function() {
                if (material.form.op_type == 'in') {
                    materialIF.operateIn(newObj).success(function(data) {
                        if (data.status == 0) {
                            handleSuccess();
                        }
                    });
                } else if (material.form.op_type == 'out') {
                    materialIF.operateOut(newObj).success(function(data) {
                        if (data.status == 0) {
                            handleSuccess();
                        }
                    });
                } else {
                    materialIF.operateBuy(newObj).success(function(data) {
                        if (data.status == 0) {
                            handleSuccess();
                        }
                    });
                }
            });
        };
        listMaterial();
        listUser();
        listWarningMaterial();
        listOperateData();
    }])
    .controller('addMaterialModalController', ['$scope', '$uibModalInstance', 'materialIF', function ($scope, $uibModalInstance, materialIF) {
        var formList = $scope.formList = [];
        var form = $scope.form = {};
        var materialList = $scope.materialList = [];

        $scope.openDate = false;
        $scope.open = function ($event) {
            $scope.openDate = true;
            $event.preventDefault();
            $event.stopPropagation();
        };
        var operateUser = $scope.operateUser = {
            user: ''
        };

        function listUser() {
            materialIF.listUser().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.userList = data.data;
                }
            });
        }
        function getMaterialUsageList () {
            materialIF.getMaterialUsageList().success(function(data) {
                if (data.status == 0 && data.data) {
                    materialList = $scope.materialList = data.data;
                }
            });
        }
        $scope.saveMaterial = function () {
            form.add_user_name_by_view = operateUser.user.username + '-' + operateUser.user.identity_code;
            form.add_user_id = operateUser.user.user_id;
            formList.push(form);
            form = $scope.form = {};
        };
        $scope.deleteMaterial = function (index) {
            formList = $scope.formList = $scope.formList.splice(0, index);
        };

        $scope.saveAllMaterial = function (index) {
            materialIF.saveMaterial({
                target_user_id: operateUser.user.user_id,
                materials: JSON.stringify(formList)
            }).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('操作成功！');
                } else {
                    $uibModalInstance.close();
                }
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        listUser();
        getMaterialUsageList();
    }])
    .controller('confirmMaterialDataModalController', ['$scope', '$uibModalInstance', 'item', '$rootScope', function ($scope, $uibModalInstance, item, $rootScope) {
        console.log(item);
        $scope.newMaterial = item.newMaterial;
        $scope.operateType = item.operateType;

        $scope.comfirm = function () {
            $uibModalInstance.close();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .service('confirmMaterialDataModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: confirmDataModal,
                    controller: 'confirmMaterialDataModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('materialModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function () {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: addMaterialModal,
                    controller: 'addMaterialModalController',
                    size: 'lg'
                });
            }
        }
    }])
    .service('materialIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var materialIF = {
            listMaterial: function (params) {
                return $http.get('/material/api/getlist', {
                    params: params
                });
            },
            listWarningMaterial: function (params) {
                return $http.get('/material/api/stocklist', {
                    params: params
                });
            },
            getMaterialUsageList: function (params) {
                return $http.get('/material/api/getMaterialUsageList', {
                    params: params
                });
            },
            listOperateData: function (params) {
                return $http.get('/material/api/history', {
                    params: params
                });
            },
            operateIn: function (params) {
                return $http.post('/material/api/withdraw', params);
            },
            operateOut: function (params) {
                return $http.post('/material/api/deduct', params);
            },
            operateBuy: function (params) {
                return $http.post('/material/api/buy', params);
            },
            saveMaterial: function (params) {
                return $http.post('/material/api/add', params);
            }

        };
        return angular.extend({}, commonData, materialIF);
    }])
    .directive('chooseItem', ['$compile', function($compile) {
        return {
            // link: function(scope, ele, attr) {
            //     if(scope.$last) {
            //         scope.$eval(function() {
            //             //如果引入jquery和zepto时候的写法
            //             //$('li').bind('click', function() {
            //             //$(this).toggleClass('choosed')
            //             //console.log('toggleClass')
            //             //})
            //             // var content = $compile(template)(scope);
            //             // ele.append(content);
            //             // console.log(ele);
            //
            //             // angular.element(document.querySelectorAll('button')).bind('click', function() {
            //             //     angular.element(this).toggleClass('choosed')
            //             //     console.log('in');
            //             // });
            //         }())
            //     }
            // }
            scope: {
                data: "=data"
            },
            restrict: "E",
            link: function( scope, element, attrs ) {
                // 检测数据
                scope.$watch(function() {
                    return scope.data;
                }, function() {
                    scope.newMaterial = scope.data;
                });

                $scope.deleteMaterial = function (index) {
                    newMaterial = $scope.newMaterial = $scope.newMaterial.splice(index, 1);
                };
            },
            templateUrl: newMaterial
        }
    }])
;
