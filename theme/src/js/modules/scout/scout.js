/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var moment = require('moment');
moment.locale('zh-cn');

var pageUrl = require('./scout.html');
var listHistoryScoutModal = require('./listHistoryScoutModal.html');
angular
    .module('app.scout', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/scout', {
                templateUrl: pageUrl,
                controller: 'scoutController',
                state: 'scout'
            })
        ;
    }])
    .controller('scoutController', ['$scope', '$routeParams', 'addPicModal', 'listHistoryScoutModal', 'scoutIF', 'FileUploader', 'Notify', 'Alert', function($scope, $routeParams, addPicModal, listHistoryScoutModal, scoutIF, FileUploader, Notify, Alert) {

        var form = $scope.form = {
            damage_condition: 0,
            scout_image: '',
            scout_date: moment().format('L')
        };
        // 今日巡视记录
        var todayScoutList = $scope.todayScoutList = [];

        $scope.pageInfo = {
            roadCodeList: [], // 巡视路名
            startNo: [], // 起点桩号
            endNo: [], // 终点桩号
            laneNum: [], // 车道路号
            scoutFacility: [], // 设施类型
            scoutDamage: [], // 损坏类型
            damageCondition: [], // 损坏程度列表
            myMaterialAndDevice: [] // 巡视车辆

        };
        // 是否编辑
        $scope.isUpdate = false;
        // 查询时间
        $scope.searchTime = {
            stTime: moment().subtract(7, 'days').format('L'),
            etTime: moment().format('L')
        };
        function listToday() {
            scoutIF.listToday().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.todayScoutList = data.data.list;
                }
            });
        }

        function listRoadCodeList() {
            scoutIF.listRoadCodeList('scout').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.roadCodeList = data.data;
                }
            });
        }

        function listStartNo() {
            scoutIF.listStartNo(form.road_code, 'scout').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.startNo = data.data;
                }
            });
        }

        function listEndNo() {
            scoutIF.listEndNo(form.road_code, 'scout').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.endNo = data.data;
                }
            });
        }

        function listLaneNu() {
            scoutIF.listLaneNu(form.road_code, 'scout').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.laneNum = data.data;
                }
            });
        }

        function listScoutFacility() {
            scoutIF.listScoutFacility().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.scoutFacility = data.data;
                }
            });
        }

        function listScoutDamage() {
            scoutIF.listScoutDamage(form.facility_id).success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.scoutDamage = data.data;
                }
            });
        }

        function listDamageCondition() {
            scoutIF.listDamageCondition(form.facility_id, form.damage_type).success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.damageCondition = data.data;
                }
            });
        }

        function listMyMaterialAndDevice() {
            scoutIF.listMyMaterialAndDevice('scout').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.myMaterialAndDevice = data.data.device_list;
                }
            });
        }

        function listPageInfo() {
            listRoadCodeList();
            listScoutFacility();
            listMyMaterialAndDevice();
        }

        $scope.toAddPic = function () {
            addPicModal.exec().result.then(function() {

            });
        };

        // 改变巡视路名
        $scope.changeRoad = function (isReset) {
            if (isReset) {
                form.start_no = '';
                form.end_no = '';
                form.lane_no = '';
            }
            listStartNo();
            listEndNo();
            listLaneNu();
        };

        $scope.changeFacility = function (isReset) {
            if (isReset) {
                form.damage_type = '';
                form.damage_condition = '';
            }
            listScoutDamage();
        };

        $scope.changeDamage = function (isReset) {
            if (isReset) {
                form.damage_condition = '';
            }
            listDamageCondition();
        };

        // 选择时间
        $scope.status = {};
        $scope.startDateOptions = {
            maxDate: moment(),
            change: function () {
                $scope.endDateOptions.minDate = $scope.searchTime.stTime;
            }
        };
        $scope.endDateOptions = {
            minDate: moment().subtract(7, 'days'),
            change: function () {
                $scope.startDateOptions.maxDate = new Date($scope.searchTime.etTime);
            }
        };
        $scope.open = function ($event, type) {
            $scope.status[type + 'Opened'] = true;
            $event.preventDefault();
            $event.stopPropagation();
        };

        // 上传文件
        var uploader = $scope.uploader = new FileUploader({
            url: '/upload?fileName=file',
            alias: 'file',
            queueLimit: 1,
            autoUpload: true
        });
        uploader.onSuccessItem = function (item, response, status, headers) {
            if (response.status == 0) {
                form.scout_image = response.data
            }
        };

        $scope.saveScout = function () {
            if (!form.scout_image) {
                Alert.alert('请先上传图片', 'danger');
            }
            if ($scope.isUpdate) {
                delete form.$$hashKey;
                scoutIF.editScout(form).success(function(data) {
                    if (data.status == 0) {
                        listToday();
                        form = $scope.form = {};
                        Alert.alert('操作成功！');
                    }
                });
                $scope.isUpdate = false;
            } else {
                scoutIF.addScout(form).success(function(data) {
                    if (data.status == 0) {
                        form = $scope.form = {};
                        listToday();
                        Alert.alert('操作成功！');
                    }
                });
            }
        };

        $scope.toEditScout = function (item) {
            $scope.isUpdate = true;
            form = $scope.form = item;
            $scope.changeRoad();
            $scope.changeFacility();
            $scope.changeDamage();
        };

        $scope.cancelUpdateScout = function () {
            form = $scope.form = {};
            $scope.isUpdate = false;
        };

        $scope.searchHistoryScout = function () {
            listHistoryScoutModal.exec($scope.searchTime);
        };

        $scope.deleteScout = function(item) {
            Notify.alert('确定删除?').result.then(function () {
                scoutIF.deleteScout(item.scout_id).success(function (data) {
                    if (data.status == 0) {
                        listToday();
                    }
                });
            });
        };

        listToday();
        listPageInfo();
    }])
    .controller('listHistoryScoutModalController', ['$scope', '$uibModalInstance', 'item', 'scoutIF', function ($scope, $uibModalInstance, item, scoutIF) {
        $scope.list = [];
        $scope.item = item;
        var form = $scope.form = {};
        $scope.checkScout = function (item) {
            scoutIF.searchScoutById(item.scout_id).success(function(data) {
                if (data.status == 0) {
                    form = $scope.form = data.data[0];
                }
            });
        };
        function list() {
            scoutIF.searchHistoryScout(item).success(function(data) {
                if (data.status == 0) {
                    $scope.list = data.data.list || [];
                }
            });
        }
        $scope.search = function () {
            list();
        };
        $scope.reset = function () {
            $scope.item.start_no = '';
            list();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        list();
    }])
    .service('scoutIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var deviceIF = {
            listToday: function () {
                return $http.get('/scout/api/today');
            },
            searchHistoryScout: function (params) {
                return $http.get('/scout/api/history', {
                    params: params
                });
            },
            listScoutFacility: function (id) {
                return $http.get('/scout/pageinfo/getScoutFacility?facility_id=' + id);
            },
            listScoutDamage: function (id) {
                return $http.get('/scout/pageinfo/getScoutDamage?facility_id=' + id);
            },
            listDamageCondition: function (id, id2) {
                return $http.get('/scout/pageinfo/getDamageCondition?facility_id=' + id + '&damage_id=' + id2);
            },
            listOneScout: function (id) {
                return $http.get('/scout/api/getById?scout_id=' + id);
            },
            addScout: function (params) {
                return $http.post('/scout/api/add', params);
            },
            editScout: function (params) {
                return $http.post('/scout/api/edit', params);
            },
            deleteScout: function (id) {
                return $http.post('/scout/api/delete?scout_id=' + id);
            },
            searchScoutById: function (id) {
                return $http.get('/scout/api/getById?scout_id=' + id);
            }
        };
        return angular.extend({}, commonData, deviceIF);
    }])
    .service('listHistoryScoutModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: listHistoryScoutModal,
                    controller: 'listHistoryScoutModalController',
                    size: 'lg',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .filter('searchRoadName', function() { //可以注入依赖
        return function(id, data) {
            var result = '';
            for (var key in data) {
                if (data[key].road_code == id) {
                    result = data[key].road_name;
                    return result;
                }
            }
        }
    })
;





