/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var pageUrl = require('./analyzeDestroy.html');
angular
    .module('app.analyzeDestroy', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/analyze-destroy/:id', {
                templateUrl: pageUrl,
                controller: 'analyzeDestroyController',
                state: 'analyze-destroy'
            })
        ;
    }])
    // 主活动详情
    .controller('analyzeDestroyController', ['$scope', '$routeParams', 'analyzeDestroyIF', 'Notify', 'Tool', function($scope, $routeParams, analyzeDestroyIF, Notify, Tool) {
        var type = $scope.type = $routeParams.id;
        var pageInfo = $scope.pageInfo = [];
        var form = $scope.form = {};
        $scope.year = Tool.getYear();
        function list() {
            analyzeDestroyIF.list(form).success(function(data) {
                if (data.status == 0) {
                    $scope.pageData = {
                        legend: ['材料耗费（万元）', '巡视损坏数量(次)'],
                        series: [{
                                name:'材料耗费（万元）',
                                type:'bar'
                            }, {
                                name:'巡视损坏数量(次)',
                                type:'line',
                                yAxisIndex: 1
                            }
                        ],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '材料耗费',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }, {
                            type: 'value',
                            name: '损坏次数',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}次'
                            }
                        }]
                    };

                    if (data.data) {
                        // 表格数据
                        $scope.tableData = data.data[1] || [];
                        $scope.tableData2 = data.data[0] || [];
                        // 图表数据
                        $scope.pageData.series[0].data = data.data[1].data || [];
                        $scope.pageData.series[1].data = data.data[0].data || [];
                    } else {
                        // 表格数据
                        $scope.tableData = [];
                        $scope.tableData2 = [];
                        // 图表数据
                        $scope.pageData.series[0].data = [];
                        $scope.pageData.series[1].data = [];
                    }



                }
            });
        }
        function listRoadCodeList() {
            analyzeDestroyIF.listAnalyzeRoadCodeList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo = $scope.pageInfo = data.data;
                    form.year =  String(new Date().getFullYear());
                    form.road_name = pageInfo[0].road_name || '';
                    form.type =  type == 1 ? 'crack' : 'surface';
                    list();
                }
            });
        }

        $scope.search = function () {
            list();
        };
        listRoadCodeList();
    }])
    .service('analyzeDestroyIF', ['$http', 'commonData', 'Tool', function($http, commonData) {
        var userIF = {
            list: function (params) {
                return $http.get('/stat/typical-damage-analyze/api/getReportData', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;





