/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var moment = require('moment');
moment.locale('zh-cn');

var pageUrl = require('./scene.html');
var addPicModal = require('./addPicModal.html');
var listHistorySceneModal = require('./listHistorySceneModal.html');
angular
    .module('app.scene', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/scene', {
                templateUrl: pageUrl,
                controller: 'sceneController',
                state: 'scene'
            })
        ;
    }])
    .controller('sceneController', ['$scope', '$routeParams', 'addPicModal', 'listHistorySceneModal', 'sceneIF', 'Alert', 'Notify', 'FileUploader', function($scope, $routeParams, addPicModal, listHistorySceneModal, sceneIF, Alert, Notify, FileUploader) {

        $scope.userList = [];
        // 操作用户
        var select = $scope.select = {
            users: [],
            devices: []
        };

        // 巡视历史记录
        var scoutList = $scope.scoutList = {
            data: [],
            form: {
                offset: 1,
                count: 5,
                stTime: moment().subtract(7, 'days').format('L'),
                etTime: moment().format('L')
            }
        };

        // 养护记录查询
        $scope.sceneSearch = {
            stTime: moment().subtract(7, 'days').format('L'), //moment().subtract(7, 'days').format('L'), 2017-04-04
            etTime: moment().format('L')
        };

        //页面配置信息
        $scope.pageInfo = {
            myMaterialAndDevice: [] // 设备和材料

        };

        // 页面表单
        var form = $scope.form = {
            scene_type: '-1'
        };

        // 被选中的巡视记录
        var newScene = $scope.newScene = [];

        // 当前执行添加图片的item
        var currentItem = -1;

        // 是否需要选中多选框
        $scope.isShowChecker = true;

        // 页面配置信息
        $scope.pageInfo = {
            roadCodeList: [], // 巡视路名
            startNo: [], // 起点桩号
            endNo: [], // 终点桩号
            laneNum: [] // 车道路号
        };
        // 分包公司列表
        $scope.laborList = [];

        // 临时存储除【日常】模式以外的图片，多张的。
        var shortImages = $scope.shortImages = [];

        $scope.toAddPic = function (item, index) {
            currentItem = index;
            item.scene_type = form.scene_type;
            addPicModal.exec(item).result.then(function(result) {
                newScene[index] = angular.extend({}, newScene[index], result);
            });
        };

        $scope.toShowPic = function (item, index) {
            item.isUpdate = true;
            addPicModal.exec(item).result.then(function(result) {
                newScene[index] = angular.extend({}, newScene[index], result);
            });
        };

        function listHistoryScoutList() {
            sceneIF.listHistoryScoutList(scoutList.form).success(function(data) {
                if (data.status == 0) {
                    scoutList.data = data.data.list || [];
                    checkScenelLoad();
                    scoutList.form.total = data.data.total;
                }
            });
        }

        function listUser() {
            sceneIF.listUser().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.userList = data.data;
                }
            });
        }

        function listMyMaterialAndDevice() {
            sceneIF.listMyMaterialAndDevice('scene').success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo.myMaterialAndDevice = data.data;
                }
            });
        }
        function listLabor() {
            sceneIF.listLabor().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.laborList = data.data;
                }
            });
        }

        function listRoadCodeList() {
            sceneIF.listRoadCodeList('scene').success(function(data) {
                if (data.status == 0) {
                    $scope.pageInfo.roadCodeList = data.data || [];
                }
            });
        }

        function listStartNo() {
            sceneIF.listStartNo(form.road_code, 'scene').success(function(data) {
                if (data.status == 0) {
                    $scope.pageInfo.startNo = data.data || [];
                }
            });
        }

        function listEndNo() {
            sceneIF.listEndNo(form.road_code, 'scene').success(function(data) {
                if (data.status == 0) {
                    $scope.pageInfo.endNo = data.data || [];
                }
            });
        }

        function listLaneNu() {
            sceneIF.listLaneNu(form.road_code, 'scene').success(function(data) {
                if (data.status == 0) {
                    $scope.pageInfo.laneNum = data.data || [];
                }
            });
        }

        // 选择时间
        $scope.status = {};
        $scope.sceneStartDateOptions = {
            maxDate: moment(),
            change: function () {
                $scope.sceneEndDateOptions.minDate = $scope.sceneSearch.stTime;
            }
        };
        $scope.sceneEndDateOptions = {
            minDate: moment().subtract(7, 'days'),
            change: function () {
                $scope.sceneStartDateOptions.maxDate = new Date($scope.sceneSearch.etTime);
            }
        };

        $scope.scoutStartDateOptions = {
            maxDate: moment(),
            change: function () {
                $scope.scoutEndDateOptions.minDate = $scope.scoutList.form.stTime;
            }
        };
        $scope.scoutEndDateOptions = {
            minDate: moment().subtract(7, 'days'),
            change: function () {
                $scope.scoutStartDateOptions.maxDate = new Date($scope.scoutList.form.etTime);
            }
        };
        $scope.open = function ($event, type) {
            $scope.status[type + 'Opened'] = true;
            $event.preventDefault();
            $event.stopPropagation();
        };

        // 删除的时候， 把先前选中的项去掉。
        function checkSceneDelete(id) {
            var oldData = scoutList.data || [];
            var oldDataItem = null;
            for (var i = 0; i < oldData.length; i++) {
                oldDataItem = oldData[i];
                if (oldDataItem.scout_id == id) {
                    oldDataItem.isChecked = false;
                }
            }
        }
        // 页面加载的时候判断是否有选中的项
        function checkScenelLoad() {
            var oldData = scoutList.data || [];
            var newData = newScene || [];
            var oldDataItem = null;
            var newDataItem = null;
            for (var i = 0; i < oldData.length; i++) {
                oldDataItem = oldData[i];
                if (newData.length == 0) {
                    oldDataItem.isChecked = false;
                }
                for (var j = 0; j < newData.length; j++) {
                    newDataItem = newData[j];
                    if (oldDataItem.scout_id == newDataItem.scout_id) {
                        oldDataItem.isChecked = true;
                    }
                }
            }
        }

        $scope.checkScout = function ($event, item) {
            var checkbox = $event.target;

            if (!form.scene_type || form.scene_type == -1) {
                checkbox.checked = false;
                Alert.alert('请先选择【养护类别】', 'danger');
                return;
            }
            var action = (checkbox.checked ? 'add' : 'remove');
            item.isChecked = true;

            if (action == 'add') {
                var shortScout = angular.extend({}, item);
                newScene.push(shortScout);

            }
        };
        $scope.removeScene = function (index, id) {
            if (newScene.length == 1) {
                newScene = $scope.newScene = [];
            } else {
                $scope.newScene.splice(index, 1);
            }
            checkSceneDelete(id);
        };
        $scope.searchHistoryScene = function () {
            listHistorySceneModal.exec($scope.sceneSearch);
        };
        $scope.searchScout = function () {
            listHistoryScoutList();
        };

        $scope.resetSearchScout = function () {
            scoutList.form.stTime = '';
            scoutList.form.etTime = '';
            listHistoryScoutList();
        };

        function handleSuccess() {
            Alert.alert('维修记录已添加成功！');
            form = $scope.form = {};
            select.users = $scope.select.users = [];
            select.devices = $scope.select.devices = [];
            scoutList.form.offset = 1;
            listHistoryScoutList();
        }
        $scope.saveScene = function () {

            if (!select.users.length) {
                Alert.alert('请选择养护操作人员！', 'danger');
                return;
            }

            // 判断是选择了巡视记录
            if (newScene.length == 0 && form.scene_type == 1) {
                Alert.alert('请选择巡视记录！', 'danger');
                return;
            }

            // 巡视ID
            var scoutId = [];
            // 巡视ID对应的图片对象
            var  imageJson = {};
            // 日常模式是否上传图片
            var isUploadPic = true;
            for (var key2 in newScene) {
                if (!newScene[key2].scene_image_list) {
                    isUploadPic = false;
                }
                imageJson[newScene[key2].scout_id] = {
                    scene_image_list: newScene[key2].scene_image_list,
                    image_desc: newScene[key2].image_desc,
                    scene_remark: newScene[key2].scene_remark
                };
                scoutId.push(newScene[key2].scout_id);
            }

            // 判断是否选择了上传图片
            if (!isUploadPic && form.scene_type == 1) {
                Alert.alert('请选择图片', 'danger');
                return false;
            }

            // 非日常模式下面判断是否选择图片
            if (form.scene_type != 1 && !shortImages.length) {
                Alert.alert('请选择图片', 'danger');
                return false;
            }

            if (!select.devices.length) {
                Alert.alert('请选择设备', 'danger');
                return false;
            }

            // 判断是否选择了材料未选择材料用量
            var newMaterial = $scope.pageInfo.myMaterialAndDevice.material_list;
            var newMaterialString = [];
            var isSuccess = true;
            var isCheckOne = false;

            if (form.scene_type != 3) {
                for (var key in newMaterial) {
                    if (newMaterial[key].checked && !newMaterial[key].cost_num) {
                        isSuccess = false;
                    } else {
                        if (newMaterial[key].checked) {
                            isCheckOne = true;
                            newMaterialString.push({
                                material_id: newMaterial[key].material_id,
                                cost_num: newMaterial[key].cost_num
                            });
                        }
                    }
                }
            }

            if (form.scene_type == 1 && (!isSuccess || !isCheckOne)) {
                Alert.alert('请选择【材料】并输入对应的【材料用量】', 'danger');
                return false;
            }

            var users = select.users.map(function (value, index, array) {
                return value.user_id;
            });

            var devices = select.devices.map(function (value, index, array) {
                return value.device_id;
            });



            form.scout_id_list = scoutId.join(',');
            form.user_id_list = users.join(',');
            form.device_id_list = devices.join(',');
            form.materialJson = JSON.stringify(newMaterialString);
            form.imageJson = JSON.stringify(imageJson);
            form.scene_image_list = shortImages.join('#');

            if (form.scene_type == 1) {
                delete form.road_code;
                delete form.start_no;
                delete form.end_no;
                delete form.lane_num;
                delete form.scene_image_list;
                delete form.image_desc;
                delete form.scene_remark;
            } else {
                delete form.imageJson;
                delete form.scout_id_list;
                delete form.materialJson;
            }
            console.log(form);
            if (form.scene_type == 1) {
                sceneIF.addNormal(form).success(function(data) {
                    if (data.status == 0) {
                        newScene = $scope.newScene = [];
                        handleSuccess();
                    }
                });
            } else if (form.scene_type == 2) {
                sceneIF.addSpecific(form).success(function(data) {
                    if (data.status == 0) {
                        handleSuccess();
                        shortImages = $scope.shortImages = [];
                    }
                });
            } else {
                sceneIF.addClean(form).success(function(data) {
                    if (data.status == 0) {
                        handleSuccess();
                        shortImages = $scope.shortImages = [];
                    }
                });
            }

        };

        $scope.pageChangedScoutList = function () {
            listHistoryScoutList();
        };

        $scope.changeSceneType = function () {
            if (form.scene_type == 1) {
                $scope.isShowChecker = true;
            } else {
                $scope.isShowChecker = false;
            }
            newScene = $scope.newScene = [];
        };

        // 上传文件
        var uploader = $scope.uploader = new FileUploader({
            url: '/upload?fileName=file',
            alias: 'file',
            queueLimit: 3,
            autoUpload: true
        });
        uploader.onSuccessItem = function (item, response, status, headers) {
            if (response.status == 0) {
                shortImages.push(response.data);
            }
        };
        $scope.removePic = function (index) {
            shortImages.splice(index, 1);
        };

        $scope.changeRoad = function (isReset) {
            form.start_no = '';
            form.end_no = '';
            form.lane_no = '';
            listStartNo();
            listEndNo();
            listLaneNu();
        };
        listUser();
        listLabor();
        listRoadCodeList();
        listMyMaterialAndDevice();
        listHistoryScoutList();
    }])
    // 日常模式下面的图片弹窗
    .controller('addPicModalController', ['$scope', '$uibModalInstance', 'item', 'FileUploader', 'Alert', function ($scope, $uibModalInstance, item, FileUploader, Alert) {
        $scope.info = item;
        var form = $scope.form = {
            scene_image_list: ''
        };
        $scope.imagesTip = false; // 图片提示
        var images = $scope.images = [];

        // 是否是更新状态
        if (item.isUpdate) {
            images = $scope.images = item.scene_image_list.split('#');
            form.image_desc = item.image_desc;
            form.scene_remark = item.scene_remark;
        }
        // 上传文件
        var uploader = $scope.uploader = new FileUploader({
            url: '/upload?fileName=file',
            alias: 'file',
            queueLimit: 3,
            autoUpload: true
        });
        uploader.onSuccessItem = function (item, response, status, headers) {
            if (response.status == 0) {
                images.push(response.data);
            } else {
                Alert.alert('上传图片失败', 'danger');
            }
        };
        $scope.removePic = function (index) {
            images.splice(index, 1);
        };
        $scope.savePic = function () {
            if (!images.length) {
                $scope.imagesTip = true;
            }
            $scope.imagesTip = false;
            form.scene_image_list = images.join('#');

            $uibModalInstance.close(form);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('listHistorySceneModalController', ['$scope', '$uibModalInstance', 'sceneIF', 'item', function ($scope, $uibModalInstance, sceneIF, item) {
        $scope.item = item;
        $scope.list = [];
        var form = $scope.form = {};
        $scope.startNoList = [];
        $scope.checkScene = function (item) {
            sceneIF.searchSceneById(item.scene_id).success(function(data) {
                if (data.status == 0) {
                    form = $scope.form = data.data;
                }
            });
        };
        function list() {
            sceneIF.searchScene(item).success(function(data) {
                if (data.status == 0) {
                    $scope.list = data.data.list || [];
                }
            });
        }
        $scope.search = function () {
            list();
        };
        $scope.reset = function () {
            $scope.item.start_no = '';
            list();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        list()
    }])
    .service('addPicModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: addPicModal,
                    controller: 'addPicModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('sceneIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var sceneIF = {
            listHistoryScoutList: function (params) {
                return $http.get('/scene/api/getHistoryScoutList', {
                    params: params
                });
            },
            addNormal: function (params) {
                return $http.post('/scene/api/addNormal', params);
            },
            addSpecific: function (params) {
                return $http.post('/scene/api/addSpecific', params);
            },
            addClean: function (params) {
                return $http.post('/scene/api/addClean', params);
            },
            searchScene: function (params) {
                return $http.get('/scene/api/getlist', {
                    params: params
                });
            },
            searchSceneById: function (id) {
                return $http.get('/scene/api/getSceneDetailById?scene_id=' + id);
            }
        };
        return angular.extend({}, commonData, sceneIF);
    }])
    .service('listHistorySceneModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: listHistorySceneModal,
                    controller: 'listHistorySceneModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    },
                    size: 'lg'
                });
            }
        }
    }])
;
