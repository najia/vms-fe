
var templateUrl = require('./Notify.alert.html');
var echarts = require('echarts/lib/echarts');
require('echarts/theme/infographic');
// 引入柱状图
require('echarts/lib/chart/bar');
// 引入提示框和标题组件
require('echarts/lib/component/tooltip');
require('echarts/lib/component/title');

angular.module("app.common", [])
    .service('Notify', ['$uibModal', function ($uibModal) {
        return {
            alert: function (alertConfig) {
                return $uibModal.open({
                    animation: true,
                    templateUrl: templateUrl,
                    backdrop: 'static',
                    resolve: {
                        alertConfig: function () {
                            if (angular.isString(alertConfig)) {
                                alertConfig = {
                                    message: alertConfig
                                };
                            }
                            return alertConfig;
                        }
                    },
                    controller: ['$scope', '$uibModalInstance', 'alertConfig', function ($scope, $uibModalInstance, alertConfig) {
                        $scope.alertConfig = alertConfig;
                        $scope.ok = function () {
                            $uibModalInstance.close();
                        };
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }]
                })
            }
        };
    }])
    .service('Alert', [function () {
        return {
            show: false,
            type: '',
            msg: '',
            alert: function(msg, type) {
                this.show = !this.show;
                this.msg = msg;
                this.type = type || 'success';
            },
            close: function() {
                this.show = !this.show;
            }
        };
    }])
    .service('commonData', ['$http', function($http) {
        var commonData = {
            listUser: function (params) {
                return $http.get('/user/api/getCurrentOrgUserListByType', {
                    params: params
                });
            },
            listMyMaterialAndDevice: function (type) {
                return $http.get('/'+ type +'/pageinfo/getMyMaterialAndDevice');
            },
            listRoadCodeList: function (type) {
                return $http.get('/'+ type +'/pageinfo/getRoadCodeList');
            },
            listStartNo: function (id, type) {
                var paramsName = 'road_code';
                if (type == 'labor') {
                    paramsName = 'road_name'
                }
                return $http.get('/'+ type +'/pageinfo/getStartNo?'+ paramsName +'=' + id);
            },
            listEndNo: function (id, type) {
                var paramsName = 'road_code';
                if (type == 'labor') {
                    paramsName = 'road_name'
                }
                return $http.get('/'+ type +'/pageinfo/getEndNo?'+ paramsName +'=' + id);
            },
            listLaneNu: function (id, type) {
                var paramsName = 'road_code';
                if (type == 'labor') {
                    paramsName = 'road_name'
                }
                return $http.get('/'+ type +'/pageinfo/getLaneNum?'+ paramsName +'=' + id);
            },
            listAnalyzeRoadCodeList: function () {
                return $http.get('/stat/typical-damage-analyze/pageinfo/get');
            },
            listLabor: function (params) {
                return $http.get('/labor/api/getlist', {
                    params: params
                });
            },
            getMaterialUsageList: function (params) {
                return $http.get('/stat/cost-benefit-analyze/material/getMaterialUsageList', {
                    params: params
                });
            }
        };
        return commonData;
    }])
    .service('Tool', [function() {
        var tool = {
            arrayToString: function(array) {
                var result = '';
                var newArray = [];
                if (array.length) {
                    angular.forEach(array, function (v, k) {
                        newArray.push(v.user_id);
                    });
                    result = newArray.join(',');
                }
                return result;
            },
            objIsEmpty: function (obj) {
                // null and undefined are "empty"
                if (obj == null) return true;

                // Assume if it has a length property with a non-zero value
                // that that property is correct.
                if (obj.length > 0)    return false;
                if (obj.length === 0)  return true;

                // Otherwise, does it have any properties of its own?
                // Note that this doesn't handle
                // toString and valueOf enumeration bugs in IE < 9
                for (var key in obj) {
                    if (hasOwnProperty.call(obj, key)) return false;
                }

                return true;
            },
            getYear: function () {
                var date = new Date();
                var year = date.getFullYear();
                var result = [];
                for (var i = 2017; i <= year; i ++) {
                    result.push(i);
                }
                return result;
            }
        };
        return tool;
    }])
    .directive("formatDate", ['$filter', function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                function formatDate (value, format, type) {
                    var date = new Date(value);
                    if (!value || date == 'Invalid Date') {
                        return value;
                    } else {
                        return type == true ? date : $filter('date')(date, format || 'yyyy-MM-dd', '+0800')

                    }
                }
                ngModel.$formatters.push(function(value) {
                    return formatDate(value, null, true);
                });
                ngModel.$parsers.push(function(value) {
                    return formatDate(value, null, false);
                });
            }
        }
    }])
    .service('HttpHelper', [function () {
        var HttpHelper = {
            path: function (url, obj) {
                for (var key in obj) {
                    url = url.replace('{' + key + '}', obj[key]);
                }
                return url;
            }
        };
        return HttpHelper;
    }])
    .filter('identityCode', function() { //可以注入依赖
        return function(str) {
            return str.substring(14, str.length) || '';
        }
    })
    .directive('lines', ['Tool', function(Tool) {
        return {
            scope: {
                id: "@",
                data: "=data",
                title: "=title"
            },
            restrict: 'E',
            template: '<div style="width:95%; margin:0 auto; height:400px;"></div>',
            replace: true,
            link: function($scope, element, attrs, controller) {
                var option = {
                    title : {
                        text: $scope.title
                    },
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data: []
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,
                    xAxis : [],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : []
                };
                var myChart = null;
                // 检测数据
                $scope.$watch(function() {
                    return $scope.data;
                }, function() {
                    if (!Tool.objIsEmpty($scope.data)) {
                        option.series = $scope.data.series;
                        option.legend.data = $scope.data.legend;
                        option.xAxis = $scope.data.xAxis;
                        option.grid = $scope.data.grid;
                        if ($scope.data.yAxis) {
                            option.yAxis = $scope.data.yAxis;
                        }
                        if (!myChart) {
                            myChart = echarts.init(document.getElementById($scope.id), 'infographic');
                        } else {
                            myChart.clear();
                        }
                        myChart.setOption(option);
                    }
                });

            }
        };
    }])
    .directive('radar', ['Tool', function(Tool) {
        return {
            scope: {
                id: "@",
                data: "=data",
                title: "=title"
            },
            restrict: 'E',
            template: '<div style="width:95%; margin:0 auto; height:400px;"></div>',
            replace: true,
            link: function($scope, element, attrs, controller) {
                var option = {
                    title: {
                        text: $scope.title || ''
                    },
                    tooltip: {},
                    legend: {
                        data: []
                    },
                    radar: {
                        // shape: 'circle',
                        indicator: []
                    },
                    series: []
                };
                // 检测数据
                $scope.$watch(function() {
                    return $scope.data;
                }, function() {
                    if (!Tool.objIsEmpty($scope.data)) {

                        option.series = $scope.data.series;
                        option.legend.data = $scope.data.legend;
                        option.radar.indicator = $scope.data.indicator;

                        var myChart = echarts.init(document.getElementById($scope.id), 'infographic');
                        myChart.setOption(option);
                    }
                });

            }
        };
    }])
;