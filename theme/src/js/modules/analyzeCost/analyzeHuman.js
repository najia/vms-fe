/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var pageUrl = require('./analyzeHuman.html');
angular
    .module('app.analyzeHuman', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/analyze-human', {
                templateUrl: pageUrl,
                controller: 'analyzeHumanController',
                state: 'analyze-cost'
            })
        ;
    }])
    // 主活动详情
    .controller('analyzeHumanController', ['$scope', '$rootScope', '$routeParams', 'analyzeHumanIF', 'Notify', 'Tool', function($scope, $rootScope, $routeParams, analyzeHumanIF, Notify, Tool) {
        $scope.year = Tool.getYear();
        var pageInfo = $scope.pageInfo = [];
        var scoreData = $scope.scoreData = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: '',
                labor_id: ''
            },
            scoreData: {},
            costData: {}
        };
        var reportData = $scope.reportData = {
            form: {
                road_name: '',
                year: String(new Date().getFullYear())
            },
            data: {}
        };
        function list() {
            if ($rootScope.isAdmin) {
                delete scoreData.form.labor_id;
            }
            analyzeHumanIF.getScoutData(scoreData.form).success(function(data) {
                if (data.status == 0) {
                    scoreData.scoreData = {
                        legend: ['工时', '报修频次'],
                        series: [{
                            name:'工时',
                            type: $rootScope.isAdmin ? 'bar': 'line',
                            data: []
                        }, {
                            name:'报修频次',
                            type: $rootScope.isAdmin ? 'bar': 'line',
                            yAxisIndex: 1,
                            data: []
                        }],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
                            axisLabel:{
                                interval:0,
                                rotate:45,//倾斜度 -90 至 90 默认为0
                                margin:2
                            }
                        }],
                        grid: {
                            y2: 130
                        },
                        yAxis: [{
                            type: 'value',
                            name: '工时',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}小时'
                            }
                        }, {
                            type: 'value',
                            name: '报修频次',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}次'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    if ($rootScope.isAdmin) {
                        var datax = data.data.map(function (v) {
                            return v.name || ''
                        });
                        var data1 = data.data.map(function (v) {
                            return v.y_total_cost || 0
                        });
                        var data2 = data.data.map(function (v) {
                            return v.y_scout_num || 0
                        });
                        scoreData.scoreData.xAxis[0].data = datax;
                        scoreData.scoreData.series[0].data = data1 || [];
                        scoreData.scoreData.series[1].data = data2 || [];
                    } else {
                        scoreData.scoreData.series[0].data = data.data[0].data || [];
                        scoreData.scoreData.series[1].data = data.data[1].data || [];
                    }
                }
            });
            analyzeHumanIF.getCostData(scoreData.form).success(function(data) {
                if (data.status == 0) {
                    scoreData.costData = {
                        legend: ['合同总额', '公里平均'],
                        series: [{
                            name:'合同总额',
                            type:'bar',
                            data: []
                        }, {
                            name:'公里平均',
                            type:'bar',
                            data: []
                        }],
                        xAxis: [{
                            type : 'category',
                            data : [],
                            axisLabel:{
                                interval:0,
                                rotate:45,//倾斜度 -90 至 90 默认为0
                                margin:2
                            }
                        }],
                        grid: {
                            y2: 130
                        },
                        yAxis: [{
                            type: 'value',
                            name: '合同额度',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }, {
                            type: 'value',
                            name: '公里平均',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    var datax = data.data.map(function (v) {
                        return v.name || ''
                    });
                    var data1 = data.data.map(function (v) {
                        return v.y_avg_cost_length || 0
                    });
                    var data2 = data.data.map(function (v) {
                        return v.y_avg_date_length || 0
                    });
                    scoreData.costData.xAxis[0].data = datax;
                    scoreData.costData.series[0].data = data1 || [];
                    scoreData.costData.series[1].data = data2 || [];
                }
            });
        }
        function list2() {
            analyzeHumanIF.getTimeReportData(reportData.form).success(function(data) {
                if (data.status == 0) {
                    reportData.data = {
                        legend: [],
                        indicator: [],
                        series: [{
                            type: 'radar',
                            data : [{
                                value: [],
                                name: '分包公司出工对比'
                            }]
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    var indicator = data.data.map(function (v) {
                        return {
                            name: v.name || ''
                        }
                    });
                    var data = data.data.map(function (v) {
                        return v.y_total_cost || 0
                    });
                    reportData.data.indicator = indicator;
                    reportData.data.series[0].data[0].value = data || [];
                }
            });
        }
        function listRoadCodeList() {
            analyzeHumanIF.listAnalyzeRoadCodeList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo = $scope.pageInfo = data.data;
                }
            });
        }
        function listLabor() {
            analyzeHumanIF.listLabor().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.laborList = data.data;
                }
            });
        }
        $scope.search = function() {
            list();
        };
        $scope.search2 = function() {
            list2();
        };
        listLabor();
        listRoadCodeList();
        list();
        list2();
    }])
    .service('analyzeHumanIF', ['$http', 'commonData', 'Tool', function($http, commonData) {
        var userIF = {
            getScoutData: function (params) {
                return $http.get('/stat/cost-benefit-analyze/labor/getPerformanceReportData', {
                    params: params
                });
            },
            getCostData: function (params) {
                return $http.get('/stat/cost-benefit-analyze/labor/getCostReportData', {
                    params: params
                });
            },
            getTimeReportData: function (params) {
                return $http.get('/stat/cost-benefit-analyze/labor/getTimeReportData', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;





