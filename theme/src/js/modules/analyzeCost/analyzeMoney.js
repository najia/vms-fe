/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var pageUrl = require('./analyzeMoney.html');
angular
    .module('app.analyzeMoney', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/analyze-money', {
                templateUrl: pageUrl,
                controller: 'analyzeMoneyController',
                state: 'analyze-cost'
            })
        ;
    }])
    // 主活动详情
    .controller('analyzeMoneyController', ['$scope', '$rootScope', '$routeParams', 'analyzeMoneyIF', 'Notify', 'Tool', function($scope, $rootScope, $routeParams, analyzeMoneyIF, Notify, Tool) {
        $scope.year = Tool.getYear();
        var pageInfo = $scope.pageInfo = {
            roadList: [],
            typeList: [{
                name: '总计',
                value: 14
            }, {
                name: '材料',
                value: 2
            }, {
                name: '机械',
                value: 4
            }, {
                name: '人力',
                value: 8
            }]
        };
        var typeList = $scope.typeList = {
            data: [{
                name: '总计',
                value: 14
            }]
        };
        var typeChoose = $scope.typeChoose = '1';
        var accumulateCostReport = $scope.accumulateCostReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: '',
                type: 14
            },
            data: {}
        };
        var getAvgCostReport = $scope.getAvgCostReport = {
            form: {
                road_name: ''
            },
            data: {}
        };
        // 累计费用分析
        function getAccumulateCostReport() {
            if ($rootScope.isAdmin) {
                accumulateCostReport.form.organization_id = $rootScope.organizationId;
            }
            analyzeMoneyIF.getAccumulateCostReport(accumulateCostReport.form).success(function(data) {
                if (data.status == 0) {
                    accumulateCostReport.data = {
                        legend: ['费用'],
                        series: [{
                            name:'费用',
                            type:'line',
                            data: []
                        }],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '费用',
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    accumulateCostReport.data.series[0].data = data.data[0].data || [];
                }
            });
        }
        // 平均费用分析-月均
        function getAvgCostByTimeReport() {
            var type = 'getAvgCostByTimeReport';
            if ($scope.typeChoose == 2) {
                type = 'getAvgCostByMileReport';
            }
            analyzeMoneyIF[type](getAvgCostReport.form).success(function(data) {
                if (data.status == 0) {
                    getAvgCostReport.data = {
                        legend: ['费用'],
                        series: [{
                            name:'费用',
                            type:'bar',
                            data: [data.data.material, data.data.machine, data.data.labor]
                        }],
                        xAxis: {
                            type: 'value',
                            name: '费用',
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        },
                        yAxis: {
                            type: 'category',
                            data: ['材料','机械','人力']
                        }
                    };
                }
            });
        }

        function listRoadCodeList() {
            analyzeMoneyIF.listAnalyzeRoadCodeList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo.roadList = $scope.pageInfo.roadList = data.data;
                }
            });
        }
        function countType() {
            var totalType = 0;
            for (var key in typeList.data) {
                totalType += parseInt(typeList.data[key].value);
            }
            accumulateCostReport.form.type = totalType;
        }
        $scope.selectType = function (item) {
            var isSelectAll = item.value == 14;
            if (isSelectAll) {
                typeList.data = [{
                    name: '总计',
                    value: 14
                }];
                accumulateCostReport.form.type = 14;
            } else {
                for (var key in typeList.data) {
                    if (typeList.data[key].value == 14) {
                        typeList.data.splice(0 , 1);
                    }
                }
            }
            countType();
        };
        $scope.removeType = function () {
            if (typeList.data.length == 0) {
                typeList.data = [{
                    name: '总计',
                    value: 14
                }]
                accumulateCostReport.form.type = 14;
            }
            countType();
        };
        $scope.search = function () {
            getAccumulateCostReport();
        };
        $scope.search2 = function () {
            getAvgCostByTimeReport();
        };

        listRoadCodeList();
        getAccumulateCostReport();
        getAvgCostByTimeReport();
    }])
    .service('analyzeMoneyIF', ['$http', 'commonData', 'Tool', function($http, commonData) {
        var userIF = {
            getAccumulateCostReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/cost/getAccumulateCostReport', {
                    params: params
                });
            },
            getAvgCostByTimeReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/cost/getAvgCostByTimeReport', {
                    params: params
                });
            },
            getAvgCostByMileReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/cost/getAvgCostByMileReport', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;





