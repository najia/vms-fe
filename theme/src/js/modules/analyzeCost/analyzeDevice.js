/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var pageUrl = require('./analyzeDevice.html');
var materialParams = require('./materialParams.html');
angular
    .module('app.analyzeDevice', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/analyze-device', {
                templateUrl: pageUrl,
                controller: 'analyzeDeviceController',
                state: 'analyze-cost'
            })
        ;
    }])
    // 主活动详情
    .controller('analyzeDeviceController', ['$scope', '$rootScope', '$routeParams', 'analyzeDeviceIF', 'Tool', 'materialParams', 'Alert', function($scope, $rootScope, $routeParams, analyzeDeviceIF, Tool, materialParams, Alert) {
        $scope.editMaterialParams = function () {
            materialParams.exec().result.then(function(msg) {
                Alert.alert(msg);
            });
        };
        $scope.year = Tool.getYear();
        var pageInfo = $scope.pageInfo = {
            roadList: [],
            typeList: [{
                name: '总计',
                value: 14
            }, {
                name: '油耗',
                value: 2
            }, {
                name: '维修',
                value: 4
            }, {
                name: '折旧',
                value: 8
            }]
        };
        var typeList = $scope.typeList = {
            data: [{
                name: '总计',
                value: 14
            }]
        };
        var report = $scope.report = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: '',
                type: 14
            },
            data: {}
        };

        var deviceTypeFrequencyReport = $scope.deviceTypeFrequencyReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: ''
            },
            data: {},
            data2: {}
        };
        var deviceTypeFixReport = $scope.deviceTypeFixReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: ''
            },
            data: {},
            data2: {}
        };
        // 设备费用-里程对比
        function getReport() {
            if ($rootScope.isAdmin) {
                report.form.organization_id = $rootScope.organizationId;
            }
            analyzeDeviceIF.getReport(report.form).success(function(data) {
                if (data.status == 0) {
                    report.data = {};
                    var chartsData = {
                        legend: [],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '费用',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }, {
                            type: 'value',
                            name: '里程',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}公里'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    var legend = [];
                    var series = [];
                    for (var key in data.data) {
                        legend.push(data.data[key].name);
                        series.push({
                            name: data.data[key].name,
                            type: data.data[key].name == '里程' ? 'line' : 'bar',
                            yAxisIndex: data.data[key].name == '里程' ? 1 : 0,
                            data: data.data[key].data
                        })
                    }

                    chartsData.legend = legend;
                    chartsData.series = series;
                    report.data = chartsData;

                }
            });
        }
        // 设备使用频次对比
        function getDeviceTypeFrequencyReport() {
            if ($rootScope.isAdmin) {
                deviceTypeFrequencyReport.form.organization_id = $rootScope.organizationId;
            }
            analyzeDeviceIF.getDeviceTypeFrequencyReport(deviceTypeFrequencyReport.form).success(function(data) {
                if (data.status == 0) {
                    var datas = data.data;
                    deviceTypeFrequencyReport.data = {
                        legend: [],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [
                            {
                                type: 'value',
                                name: '使用频次',
                                axisLabel: {
                                    formatter: '{value} 次'
                                }
                            }
                        ]
                    };
                    if (!datas) {
                        return;
                    }

                    var series = datas[1].map(function (v, k) {
                        var array = [];
                        for (var key in datas) {
                            // (Math.random() * 2).toFixed(2)*100
                            array.push(datas[key][k].y_freq_value)
                        }
                        return ({
                            data: array,
                            type: 'bar',
                            stack: 'group',
                            barWidth : 20,
                            name: v.device_type_description
                        });
                    });
                    var legend = datas[1].map(function (v, k) {
                        return v.device_type_description;
                    });

                    deviceTypeFrequencyReport.data.series = series;
                    deviceTypeFrequencyReport.data.legend = legend;
                }
            });
            analyzeDeviceIF.getDeviceUsageFrequencyReport(deviceTypeFrequencyReport.form).success(function(data) {
                if (data.status == 0) {
                    var datas = data.data;
                    deviceTypeFrequencyReport.data2 = {
                        legend: [],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [
                            {
                                type: 'value',
                                name: '使用频次',
                                axisLabel: {
                                    formatter: '{value} 次'
                                }
                            }
                        ]
                    };
                    if (!datas) {
                        return;
                    }

                    var series = datas[1].map(function (v, k) {
                        var array = [];
                        for (var key in datas) {
                            array.push(datas[key][k].y_freq_value)
                        }
                        return ({
                            data: array,
                            type: 'bar',
                            stack: 'group',
                            barWidth : 20,
                            name: v.usage_name
                        });
                    });
                    var legend = datas[1].map(function (v, k) {
                        return v.usage_name;
                    });

                    deviceTypeFrequencyReport.data2.series = series;
                    deviceTypeFrequencyReport.data2.legend = legend;
                }
            });
        }
        // 设备维修对比
        function getDeviceTypeFixReport() {
            if ($rootScope.isAdmin) {
                deviceTypeFixReport.form.organization_id = $rootScope.organizationId;
            }
            analyzeDeviceIF.getDeviceTypeFixReport(deviceTypeFixReport.form).success(function(data) {
                if (data.status == 0) {
                    var datas = data.data;
                    deviceTypeFixReport.data = {
                        legend: [],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [
                            {
                                type: 'value',
                                name: '使用频次',
                                axisLabel: {
                                    formatter: '{value} 次'
                                }
                            }
                        ]
                    };
                    if (!datas) {
                        return;
                    }

                    var series = datas[1].map(function (v, k) {
                        var array = [];
                        for (var key in datas) {
                            array.push(datas[key][k].y_fix_value)
                        }
                        return ({
                            data: array,
                            type: 'bar',
                            stack: 'group',
                            barWidth : 20,
                            name: v.device_type_description
                        });
                    });
                    var legend = datas[1].map(function (v, k) {
                        return v.device_type_description;
                    });

                    deviceTypeFixReport.data.series = series;
                    deviceTypeFixReport.data.legend = legend;
                }
            });
            analyzeDeviceIF.getDeviceUsageFixReport(deviceTypeFixReport.form).success(function(data) {
                if (data.status == 0) {
                    var datas = data.data;
                    deviceTypeFixReport.data2 = {
                        legend: [],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [
                            {
                                type: 'value',
                                name: '使用频次',
                                axisLabel: {
                                    formatter: '{value} 次'
                                }
                            }
                        ]
                    };
                    if (!datas) {
                        return;
                    }

                    var series = datas[1].map(function (v, k) {
                        var array = [];
                        for (var key in datas) {
                            // test[key][k].y_freq_value
                            array.push(datas[key][k].y_fix_value)
                        }
                        return ({
                            data: array,
                            type: 'bar',
                            stack: 'group',
                            barWidth : 20,
                            name: v.usage_name
                        });
                    });
                    var legend = datas[1].map(function (v, k) {
                        return v.usage_name;
                    });

                    deviceTypeFixReport.data2.series = series;
                    deviceTypeFixReport.data2.legend = legend;
                }
            });
        }
        function listRoadCodeList() {
            analyzeDeviceIF.listAnalyzeRoadCodeList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo.roadList = $scope.pageInfo.roadList = data.data;
                }
            });
        }
        $scope.search = function() {
            getReport();
        };
        $scope.search2 = function() {
            getDeviceTypeFrequencyReport();
        };
        $scope.search3 = function() {
            getDeviceTypeFixReport();
        };

        function countType() {
            var totalType = 0;
            for (var key in typeList.data) {
                totalType += parseInt(typeList.data[key].value);
            }

            report.form.type = totalType;
        }
        $scope.selectType = function (item) {
            var isSelectAll = item.value == 14;
            if (isSelectAll) {
                typeList.data = [{
                    name: '总计',
                    value: 14
                }];
                report.form.type = 14;
            } else {
                for (var key in typeList.data) {
                    if (typeList.data[key].value == 14) {
                        typeList.data.splice(0 , 1);
                    }
                }
            }
            countType();
        };
        $scope.removeType = function (item) {
            if (typeList.data.length == 0) {
                typeList.data = [{
                    name: '总计',
                    value: 14
                }];
                report.form.type = 14;
            }
            countType();
        };
        listRoadCodeList();
        getReport();
        getDeviceTypeFrequencyReport();
        getDeviceTypeFixReport();

    }])
    .controller('materialParamsController', ['$scope', '$uibModalInstance', 'analyzeDeviceIF', 'Notify', 'Alert', function ($scope, $uibModalInstance, analyzeDeviceIF, Notify, Alert) {
        var form = $scope.form = {};
        function init() {
            analyzeDeviceIF.getMachineDict().success(function(data) {
                if (data.status == 0 && data.data) {
                    form = $scope.form = data.data;
                }
            });
        }
        $scope.save = function () {
            delete form.updated;
            analyzeDeviceIF.addMachineDict(form).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('操作成功！');
                } else {
                    $uibModalInstance.close('操作失败！');
                }
            });
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        init();
    }])
    .service('materialParams', ['$uibModal', function ($uibModal) {
        return {
            exec: function () {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: materialParams,
                    controller: 'materialParamsController'
                });
            }
        }
    }])
    .service('analyzeDeviceIF', ['$http', 'commonData', 'Tool', function($http, commonData) {
        var userIF = {
            addMachineDict: function (params) {
                return $http.post('/stat/cost-benefit-analyze/machine/addMachineDict', params);
            },
            getMachineDict: function () {
                return $http.get('/stat/cost-benefit-analyze/machine/getMachineDict');
            },
            getReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/machine/getReport1', {
                    params: params
                });
            },
            getDeviceTypeFrequencyReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/machine/getDeviceTypeFrequencyReport', {
                    params: params
                });
            },
            getDeviceUsageFrequencyReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/machine/getDeviceUsageFrequencyReport', {
                    params: params
                });
            },
            getDeviceTypeFixReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/machine/getDeviceTypeFixReport', {
                    params: params
                });
            },
            getDeviceUsageFixReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/machine/getDeviceUsageFixReport', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;





