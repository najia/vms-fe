/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var pageUrl = require('./analyzeMaterial.html');
angular
    .module('app.analyzeMaterial', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/analyze-material', {
                templateUrl: pageUrl,
                controller: 'analyzeMaterialController',
                state: 'analyze-cost'
            })
        ;
    }])
    // 主活动详情
    .controller('analyzeMaterialController', ['$scope', '$rootScope', '$routeParams', 'analyzeMaterialIF', 'Notify', 'Tool', function($scope, $rootScope, $routeParams, analyzeMaterialIF, Notify, Tool) {
        $scope.year = Tool.getYear();
        var pageInfo = $scope.pageInfo = {
            roadList: [],
            materialUsageList: [{
                material_usage_id: '',
                material_usage_name: '全部'
            }]
        };
        var material_usage_id = $scope.material_usage_id  = {
            data: [{
                material_usage_id: '',
                material_usage_name: '全部'
            }]
        };
        var costAndMileReport = $scope.costAndMileReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: '',
                material_usage_id: ''
            },
            data: {}
        };
        var topCostReport = $scope.topCostReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: ''
            },
            data: {}
        };
        function getMaterialUsageList() {
            analyzeMaterialIF.getMaterialUsageList().success(function(data) {
                if (data.status == 0) {
                    $scope.pageInfo.materialUsageList = pageInfo.materialUsageList.concat(data.data);
                    console.log($scope.pageInfo.materialUsageList);
                }
            });
        }
        // 材料费用-里程对比
        function getCostAndMileReport() {
            if ($rootScope.isAdmin) {
                costAndMileReport.form.organization_id = $rootScope.organizationId;
            }
            analyzeMaterialIF.getCostAndMileReport(costAndMileReport.form).success(function(data) {
                if (data.status == 0) {
                    costAndMileReport.data = {
                        legend: ['费用', '里程'],
                        series: [{
                            name:'费用',
                            type:'bar',
                            data: []
                        }, {
                            name:'里程',
                            type:'line',
                            yAxisIndex: 1,
                            data: []
                        }],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '费用',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }, {
                            type: 'value',
                            name: '里程',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}公里'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    costAndMileReport.data.series[0].data = data.data[0].data || [];
                    costAndMileReport.data.series[1].data = data.data[1].data || [];
                }
            });
        }
        // 高耗费材料分析
        function getTopCostReport() {
            if ($rootScope.isAdmin) {
                topCostReport.form.organization_id = $rootScope.organizationId;
            }
            analyzeMaterialIF.getTopCostReport(topCostReport.form).success(function(data) {
                if (data.status == 0) {
                    topCostReport.data = {
                        legend: ['材料名称'],
                        series: [{
                            name:'材料名称',
                            type:'bar'
                        }],
                        xAxis: [{
                            type : 'category',
                            data : []
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '费用',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }]
                    };
                    if (data.data) {
                        var datax = data.data.map(function (v) {
                            return v.name || ''
                        });
                        var data1 = data.data.map(function (v) {
                            return v.y_total_cost || 0
                        });
                        topCostReport.data.xAxis[0].data = datax || [];
                        topCostReport.data.series[0].data = data1 || [];
                    } else {
                        topCostReport.data.series[0].data = [];
                    }
                }
            });
        }
        function listRoadCodeList() {
            analyzeMaterialIF.listAnalyzeRoadCodeList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo.roadList = $scope.pageInfo.roadList = data.data;
                }
            });
        }
        $scope.search = function() {
            getCostAndMileReport();
        };
        $scope.search2 = function() {
            getTopCostReport();
        };

        function countMaterialId() {
            var materialData = material_usage_id.data.map(function (v) {
                return v.material_usage_id;
            });
            costAndMileReport.form.material_usage_id = materialData.join(',');
        }
        $scope.selectMaterial = function (item) {
            var isSelectAll = item.material_usage_id == '';
            if (isSelectAll) {
                material_usage_id.data = [{
                    material_usage_id: '',
                    material_usage_name: '全部'
                }];
                costAndMileReport.form.material_usage_id = '';
            } else {
                for (var key in material_usage_id.data) {
                    if (material_usage_id.data[key].material_usage_id == '') {
                        material_usage_id.data.splice(0 , 1);
                    }
                }
            }
            countMaterialId();

        };
        $scope.removeMaterial = function () {
            if (material_usage_id.data.length == 0) {
                material_usage_id.data = [{
                    name: '总计',
                    value: 14
                }];
                costAndMileReport.form.material_usage_id = '';
            }
            countMaterialId();
        };
        listRoadCodeList();
        getMaterialUsageList();
        getCostAndMileReport();
        getTopCostReport();
    }])
    .service('analyzeMaterialIF', ['$http', 'commonData', 'Tool', function($http, commonData) {
        var userIF = {
            getCostAndMileReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/material/getCostAndMileReport', {
                    params: params
                });
            },
            getTopCostReport: function (params) {
                return $http.get('/stat/cost-benefit-analyze/material/getTopCostReport', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;





