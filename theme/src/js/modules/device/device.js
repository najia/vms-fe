/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./device.html');
var addDeviceModal = require('./addDeviceModal.html');
var confirmDataModal = require('./confirmDataModal.html');
angular
    .module('app.device', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/device', {
                templateUrl: pageUrl,
                controller: 'deviceController',
                state: 'device'
            })
        ;
    }])
    .controller('deviceController', ['$scope', '$routeParams', 'deviceModal', 'deviceIF', 'Alert', 'Notify', 'confirmDataModal',  function($scope, $routeParams, deviceModal, deviceIF, Alert, Notify, confirmDataModal) {

        // 操作用户
        var operateUser = $scope.operateUser = {
            target_user: {
                // identity_code: '1dcf',
                // user_id: '1',
                // username: 'company'
            }
        };

        $scope.userList = [];

        // 设备
        var device = $scope.device = {
            data: [],
            form: {
                offset: 1,
                count: 5,
                device_name: '',
                target_user_id: '',
                op_type: '-1',
                device_type: '-1',
                device_usage: '-1'
            }
        };

        // 在借设备
        var borrow = $scope.borrow = {
            data: [],
            form: {
                offset: 1,
                count: 5
            }
        };

        // 损坏设备
        var fix = $scope.fix = {
            data: [],
            form: {
                offset: 1,
                count: 10
            }
        };

        // 当日操作记录
        var history = $scope.history = {
            data: [],
            form: {
                offset: 1,
                count: 5
            }
        };

        // 页面配置
        $scope.pageInfo = {};

        // 设备状态
        $scope.deviceState = ['在库', '出库', '报废', '维修'];


        // 新的设备列表
        var newDevice = $scope.newDevice = [];

        function listDevice() {
            deviceIF.listDevice(device.form).success(function(data) {
                if (data.status == 0 && data.data) {
                    device.data = data.data.list;
                    checkDeviceLoad();
                    device.form.total = data.data.total;
                }
            });
        }
        function listUser() {
            var type = {
                type: 8
            };
            deviceIF.listUser(type).success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.userList = data.data;
                }
            });
        }

        function pageInfo() {
            deviceIF.pageInfo().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.pageInfo = data.data;
                }
            });
        }

        function listBorrow() {
            deviceIF.listBorrow(borrow.form).success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.borrow.data = data.data;
                }
            });
        }

        function listFix() {
            deviceIF.listFix(fix.form).success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.fix.data = data.data;
                }
            });
        }

        function listHistory() {
            deviceIF.listHistory(history.form).success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.history.data = data.data.list;
                }
            });
        }

        // 页面加载的时候判断是否有选中的项
        function checkDeviceLoad() {
            var oldData = device.data || [];
            var newData = newDevice || [];
            var oldDataItem = null;
            var newDataItem = null;
            for (var i = 0; i < oldData.length; i++) {
                oldDataItem = oldData[i];
                if (newData.length == 0) {
                    oldDataItem.isChecked = false;
                }
                for (var j = 0; j < newData.length; j++) {
                    newDataItem = newData[j];
                    if (oldDataItem.device_id == newDataItem.device_id) {
                        oldDataItem.isChecked = true;
                    }
                }
            }
        }
        // 删除的时候， 把先前选中的项去掉。
        function checkDeviceDelete(id) {
            var oldData = device.data || [];
            var oldDataItem = null;
            for (var i = 0; i < oldData.length; i++) {
                oldDataItem = oldData[i];
                if (oldDataItem.device_id == id) {
                    oldDataItem.isChecked = false;
                }
            }
        }

        $scope.pageChangedDevice = function() {
            listDevice();
        };

        $scope.pageChangedBorrow = function() {
            listBorrow();
        };

        $scope.pageChangedFix = function() {
            listFix();
        };

        $scope.pageChangedHistory = function() {
            listHistory();
        };

        $scope.checkAddDevice = function () {
            deviceModal.exec($scope.pageInfo).result.then(function(msg) {
                listHistory();
                Alert.alert(msg);
            });
        };

        $scope.searchDevice = function () {
            device.form.offset = 1;
            listDevice();
        };
        $scope.resetSearchDevice = function () {
            device.form.offset = 1;
            device.form.device_name = '';
            listDevice();
        };

        $scope.checkUser = function () {
            device.form.offset = 1;
            device.form.target_user_id = operateUser.target_user.user_id;
            newDevice = $scope.newDevice = [];
            listDevice();
        };

        $scope.checkOperate = function () {
            if (!operateUser.target_user.user_id) {
                device.form.op_type = '-1';
                Alert.alert('请先选择【设备操作人员】', 'danger');
                return;
            }
            device.form.offset = 1;
            newDevice = $scope.newDevice = [];
            listDevice();
        };

        $scope.checkDevice = function ($event, item, index) {
            var checkbox = $event.target;
            var action = (checkbox.checked ? 'add' : 'remove');

            if (!operateUser.target_user.user_id || device.form.op_type == '-1') {
                Alert.alert('请先选择【设备操作人员】和【操作类别】', 'danger');
                checkbox.checked = false;
                return;
            }
            item.isChecked = true;
            if (action == 'add') {
                var shortDevice = angular.extend({}, item);
                newDevice.push(shortDevice);

            }
        };
        // $scope.checkOnline = function (item) {
        //     if (!item.isChecked) {
        //         item.isChecked = true;
        //     }
        // };

        $scope.deleteDevice = function (index, id) {
            if (newDevice.length == 1) {
                newDevice = $scope.newDevice = [];
            } else {
                $scope.newDevice.splice(index, 1);
            }
            checkDeviceDelete(id);
        };

        function handleSuccess() {
            newDevice = $scope.newDevice = [];
            device.form.offset = 1;
            listDevice();
            Alert.alert('操作成功！');

            borrow.form.offset = 1;
            listBorrow();
            fix.form.offset = 1;
            listFix();
            history.form.offset = 1;
            listHistory();

        }

        $scope.saveDevice = function () {
            var stringDevice = '';
            var isSuccess = true;
            if (newDevice.length == 0) {
                Alert.alert('请选择需要操作的设备', 'danger');
                return;
            }

            if (device.form.op_type == 'in') {
                stringDevice = newDevice.map(function (value, index, array) {
                    if (value.evaluation_state_id == 0) {
                        isSuccess = false;
                        return false;
                    } else {
                        return {
                            device_id: value.device_id,
                            target_state: value.target_state,
                            evaluation_id: value.evaluation_state_id
                        };
                    }
                });
            } else {
                stringDevice = newDevice.map(function (value, index, array) {
                    return {
                        device_id: value.device_id,
                    };
                });
            }
            if (!isSuccess) {
                Alert.alert('请选择质量状况！', 'danger');
                return;
            }
            var newObj = {
                target_user_id: operateUser.target_user.user_id,
                deviceJson: JSON.stringify(stringDevice)
            };
            var operateText = '报废';
            if (device.form.op_type == 'in') {
                operateText = '入库'
            } else if (device.form.op_type == 'out') {
                operateText = '出库'
            }
            var confirmData = {
                newDevice: newDevice,
                pageInfo: $scope.pageInfo,
                deviceState: $scope.deviceState,
                device: device,
                operateType: operateText
            }
            // console.log(newObj);
            confirmDataModal.exec(confirmData).result.then(function() {
                if (device.form.op_type == 'in') {
                    deviceIF.operateIn(newObj).success(function(data) {
                        if (data.status == 0) {
                            handleSuccess();
                        }
                    });
                } else if (device.form.op_type == 'out') {
                    deviceIF.operateOut(newObj).success(function(data) {
                        if (data.status == 0) {
                            handleSuccess();
                        }
                    });
                } else if (device.form.op_type == 'damage') {
                    deviceIF.operateDamage(newObj).success(function(data) {
                        if (data.status == 0) {
                            handleSuccess();
                        }
                    });
                }
            });

        };

        $scope.saveDamageDevice = function (item) {
            if (!item.fix_fee) {
                Alert.alert('请完善信息', 'danger');
                return false;
            }
            var newObj = {
                deviceJson: JSON.stringify([{
                    device_id: item.device_id,
                    target_state: item.target_state,
                    fix_fee: item.fix_fee
                }])
            };
            console.log(newObj);
            deviceIF.fixDevice(newObj).success(function(data) {
                if (data.status == 0) {
                    // 重新渲染设备列表和清楚已经存在的
                    newDevice = $scope.newDevice = [];
                    device.form.offset = 1;
                    listDevice();

                    // 重新渲染报修列表
                    fix.form.offset = 1;
                    listFix();
                    Alert.alert('操作成功！');
                }
            });
        };

        pageInfo();
        listUser();
        listFix();
        listBorrow();
        listHistory();
        listDevice();
    }])
    .controller('confirmDataModalController', ['$scope', '$uibModalInstance', 'item', '$rootScope', function ($scope, $uibModalInstance, item, $rootScope) {
        var newDevice = $scope.newDevice = item.newDevice;
        $scope.pageInfo = item.pageInfo;
        $scope.deviceState = item.deviceState;
        $scope.device = item.device;
        $scope.operateType = item.operateType;

        $scope.comfirm = function () {
            $uibModalInstance.close();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('addDeviceModalController', ['$scope', '$uibModalInstance', 'item', 'deviceIF', '$rootScope', function ($scope, $uibModalInstance, item, deviceIF, $rootScope) {
        var pageInfo = $scope.pageInfo = item;
        var formList = $scope.formList = [];
        var form = $scope.form = {
            depreciation: 0.13
        };

        $scope.openDate = false;
        $scope.open = function ($event) {
            $scope.openDate = true;
            $event.preventDefault();
            $event.stopPropagation();
        };
        var operateUser = $scope.operateUser = {
            user: ''
        };

        function listUser() {
            deviceIF.listUser().success(function(data) {
                if (data.status == 0 && data.data) {
                    $scope.userList = data.data;
                }
            });
        }

        $scope.saveDevice = function () {
            form.add_user_name_by_view = operateUser.user.username + '-' + operateUser.user.identity_code;
            form.add_user_id = operateUser.user.user_id;
            var isSelf = $rootScope.siteInfo.currentUser.organization_id == form.property;
            if (isSelf) {
                form.property_type = 3;
                form.property_type_name = '自有';
            } else {
                form.property_type = 4;
                form.property_type_name = '外来';
            }
            if (form.depreciation <= 0 || form.depreciation > 0.2) {
                alert('请输入正确的年折利率');
                return;
            }
            formList.push(form);
            form = $scope.form = {};
        };

        $scope.deleteDevice = function (index) {
            formList = $scope.formList = $scope.formList.splice(0, index);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.saveAllDevice = function () {
            console.log(formList);
            deviceIF.saveDevice({
                devices: JSON.stringify(formList)
            }).success(function(data) {
                if (data.status == 0) {
                    $uibModalInstance.close('操作成功！');
                } else {
                    $uibModalInstance.close();
                }
            });
        };

        listUser();
    }])
    .service('deviceModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: addDeviceModal,
                    controller: 'addDeviceModalController',
                    size: 'lg',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('confirmDataModal', ['$uibModal', function ($uibModal) {
        return {
            exec: function (item) {
                return $uibModal.open({
                    backdrop: 'static',
                    templateUrl: confirmDataModal,
                    controller: 'confirmDataModalController',
                    resolve: {
                        item: function () {
                            if (item) {
                                return item;
                            }
                        }
                    }
                });
            }
        }
    }])
    .service('deviceIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var deviceIF = {
            listDevice: function (params) {
                return $http.get('/device/api/getlist', {
                    params: params
                });
            },
            pageInfo: function () {
                return $http.get('/device/pageinfo/get');
            },
            operateOut: function (params) {
                return $http.post('/device/api/borrow', params);
            },
            operateIn: function (params) {
                return $http.post('/device/api/returns', params);
            },
            operateDamage: function (params) {
                return $http.post('/device/api/damage', params);
            },
            saveDevice: function (params) {
                return $http.post('/device/api/add', params);
            },
            listBorrow: function (params) {
                return $http.get('/device/api/getBorrowList', {
                    params: params
                });
            },
            listFix: function (params) {
                return $http.get('/device/api/getFixList', {
                    params: params
                });
            },
            fixDevice: function (params) {
                return $http.post('/device/api/fix', params);
            },
            listHistory: function (params) {
                return $http.get('/device/api/history', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, deviceIF);
    }])
    .filter('searchDeviceTypeName', function() { //可以注入依赖
        return function(id, data) {
            var result = '';
            for (var key in data) {
                if (data[key].device_type_id == id) {
                    result = data[key].device_type_description;
                    return result;
                }
            }
        }
    })
    .filter('searchDeviceUsageName', function() { //可以注入依赖
        return function(id, data) {
            var result = '';
            for (var key in data) {
                if (data[key].device_usage_id == id) {
                    result = data[key].usage_name;
                    return result;
                }
            }
        }
    })
    .filter('searchDeviceEvaluationName', function() { //可以注入依赖
        return function(id, data) {
            var result = '';
            for (var key in data) {
                if (data[key].evaluation_id == id) {
                    result = data[key].evaluation_name;
                    return result;
                }
            }
        }
    })
    .filter('searchDeviceStateName', function() { //可以注入依赖
        return function(id, data) {
            var result = '';
            for (var key in data) {
                if (data[key].device_state_id == id) {
                    result = data[key].device_state_name;
                    return result;
                }
            }
        }
    })
    .filter('searchPropertyName', function() { //可以注入依赖
        return function(id, data) {
            var result = '';
            for (var key in data) {
                if (data[key].organization_id == id) {
                    result = data[key].organization_description;
                    return result;
                }
            }
        }
    })
;
