/**
 * Created by Jessie on 16/11/16.
 */

'use strict';

var pageUrl = require('./daily.html');
angular
    .module('app.daily', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/daily/:id', {
                templateUrl: pageUrl,
                controller: 'dailyController',
                state: 'daily'
            })
        ;
    }])
    // 主活动详情
    .controller('dailyController', ['$scope', '$routeParams', 'dailyIF', function($scope, $routeParams, dailyIF) {
        var type = $scope.type = $routeParams.id;
        var listData = $scope.listData = {
            data: [],
            form: {
                offset: 1,
                count: 20
            }
        };
        function list() {
            var IF = type == 1 ? 'listLogin' : 'listOperate';
            dailyIF[IF](listData.form).success(function(data) {
                if (data.status == 0) {
                    listData.data = $scope.listData.data = data.data.list || [];
                    listData.form.total = data.data.total;
                }
            });
        }

        $scope.pageChanged = function() {
            list();
        };

        list();
    }])
    .service('dailyIF', ['$http', 'commonData', 'Tool', function($http, commonData, Tool) {
        var dailyIF = {
            listLogin: function (params) {
                return $http.get('/user/api/getLoginLogList', {
                    params: params
                });
            },
            listOperate: function (params) {
                return $http.get('/user/api/getOpLogList', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, dailyIF);
    }])
;





