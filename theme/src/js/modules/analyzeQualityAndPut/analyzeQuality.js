/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var pageUrl = require('./analyzeQuality.html');
angular
    .module('app.analyzeQuality', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/analyze-quality', {
                templateUrl: pageUrl,
                controller: 'analyzeQualityController',
                state: 'analyze-quality-and-put'
            })
        ;
    }])
    // 主活动详情
    .controller('analyzeQualityController', ['$scope', '$rootScope', '$routeParams', 'analyzeQualityIF', 'Notify', 'Tool', function($scope, $rootScope, $routeParams, analyzeQualityIF, Notify, Tool) {
        $scope.year = Tool.getYear();
        var pageInfo = $scope.pageInfo = {
            roadList: []
        };
        var costEffectAndCostReport = $scope.costEffectAndCostReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: ''
            },
            data: {}
        };
        var scoutEffectReport = $scope.scoutEffectReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: ''
            },
            data: {}
        };
        // 养护效果于投入分析
        function getCostEffectAndCostReport() {
            if ($rootScope.isAdmin) {
                costEffectAndCostReport.form.organization_id = $rootScope.organizationId;
            }
            analyzeQualityIF.getCostEffectAndCostReport(costEffectAndCostReport.form).success(function(data) {
                if (data.status == 0) {
                    costEffectAndCostReport.data = {
                        legend: ['里程', '费用'],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '里程',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}公里'
                            }
                        }, {
                            type: 'value',
                            name: '费用',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    costEffectAndCostReport.data.series = data.data.map(function (v, k) {
                        return {
                            name: v.name,
                            type: 'line',
                            data: v.data,
                            yAxisIndex: k
                        }
                    });
                }
            });
        }
        // 养护效果评价
        function getScoutEffectReport() {
            if ($rootScope.isAdmin) {
                scoutEffectReport.form.organization_id = $rootScope.organizationId;
            }
            analyzeQualityIF.getScoutEffectReport(scoutEffectReport.form).success(function(data) {
                if (data.status == 0) {
                    scoutEffectReport.data = {
                        legend: ['巡视损坏', '合理上界', '合理下界'],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '里程',
                            axisLabel: {
                                formatter: '{value}公里'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    scoutEffectReport.data.series = data.data.map(function (v) {
                        return {
                            name: v.name,
                            type: 'line',
                            data: v.data
                        }
                    });
                }
            });
        }
        function listRoadCodeList() {
            analyzeQualityIF.listAnalyzeRoadCodeList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo.roadList = $scope.pageInfo.roadList = data.data;
                }
            });
        }
        $scope.search = function () {
            getCostEffectAndCostReport();
        };
        $scope.search2 = function () {
            getScoutEffectReport();
        };
        listRoadCodeList();
        getCostEffectAndCostReport();
        getScoutEffectReport();
    }])
    .service('analyzeQualityIF', ['$http', 'commonData', 'Tool', function($http, commonData) {
        var userIF = {
            getCostEffectAndCostReport: function (params) {
                return $http.get('/stat/scout-quatity-analyze/quality/getCostEffectAndCostReport', {
                    params: params
                });
            },
            getScoutEffectReport: function (params) {
                return $http.get('/stat/scout-quatity-analyze/quality/getScoutEffectReport', {
                    params: params
                });
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;





