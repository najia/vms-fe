/**
 * Created by Jessie on 16/11/16.
 */

'use strict';
var pageUrl = require('./analyzePut.html');
angular
    .module('app.analyzePut', [])
    .config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/analyze-put', {
                templateUrl: pageUrl,
                controller: 'analyzePutController',
                state: 'analyze-quality-and-put'
            })
        ;
    }])
    // 主活动详情
    .controller('analyzePutController', ['$scope', '$rootScope', '$routeParams', 'analyzePutIF', 'Notify', 'Tool', function($scope, $rootScope, $routeParams, analyzePutIF, Notify, Tool) {
        $scope.year = Tool.getYear();
        var pageInfo = $scope.pageInfo = {
            roadList: [],
            facilityList: []
        };
        var sceneByTypeReport = $scope.sceneByTypeReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: ''
            },
            data: {}
        };
        var scoutEffectReport = $scope.scoutEffectReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: ''
            },
            data: {}
        };
        var sceneDamageReport = $scope.sceneDamageReport = {
            form: {
                year: String(new Date().getFullYear()),
                road_name: '',
                facility_id: ''
            },
            data: []
        };
        // 养护类别与投入分析
        function getSceneByTypeReport() {
            analyzePutIF.getSceneByTypeReport(sceneByTypeReport.form).success(function(data) {
                if (data.status == 0) {
                    var chartsData = {
                        legend: [],
                        series: [],
                        xAxis: [{
                            type : 'category',
                            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '费用',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}万元'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    var legend = [];
                    var series = [];
                    for (var key in data.data) {
                        legend.push(data.data[key].name);
                        series.push({
                            name:  data.data[key].name,
                            type: 'line',
                            data: data.data[key].data
                        });
                    }
                    chartsData.legend = legend;
                    chartsData.series = series;
                    console.log(chartsData);
                    sceneByTypeReport.data = chartsData;
                }
            });
        }
        // 养护类别与投入分析
        function getSceneDamageReport(countFacility) {
            sceneDamageReport.form.facility_id = pageInfo.facilityList[countFacility].facility_id;
            analyzePutIF.getSceneDamageReport(sceneDamageReport.form).success(function(data) {
                if (data.status == 0) {
                    var chartsData = {
                        legend: ['损坏频次', '作业量'],
                        series: [{
                            name: '损坏频次',
                            type: 'bar',
                            data: []

                        }, {
                            name: '作业量',
                            type: 'line',
                            data: [],
                            yAxisIndex: 1

                        }],
                        xAxis: [{
                            type : 'category',
                            data : []
                        }],
                        yAxis: [{
                            type: 'value',
                            name: '损坏频次',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}次'
                            }
                        }, {
                            type: 'value',
                            name: '作业量',
                            min: 0,
                            axisLabel: {
                                formatter: '{value}'
                            }
                        }]
                    };
                    if (!data.data) {
                        return;
                    }
                    var datax = [];
                    var data1 = [];
                    var data2 = [];
                    for (var key in data.data) {
                        datax.push(data.data[key].name);
                        data1.push(data.data[key].y_damage_num)
                        data2.push(data.data[key].y_total_work)
                    }
                    chartsData.xAxis[0].data = datax;
                    chartsData.series[0].data = data1;
                    chartsData.series[1].data = data2;

                    sceneDamageReport.data.push(chartsData);
                    countFacility += 1;
                    if (countFacility == pageInfo.facilityList.length) {
                        return;
                    }
                    getSceneDamageReport(countFacility)
                }
            });
        }
        function listRoadCodeList() {
            analyzePutIF.listAnalyzeRoadCodeList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo.roadList = $scope.pageInfo.roadList = data.data;
                }
            });
        }
        function getFacilityList() {
            analyzePutIF.getFacilityList().success(function(data) {
                if (data.status == 0 && data.data) {
                    pageInfo.facilityList = $scope.pageInfo.facilityList = data.data;
                    getSceneDamageReport(0);
                }
            });
        }
        $scope.search = function () {
            getSceneByTypeReport();
        };
        $scope.search2 = function () {
            getSceneDamageReport(0);
        };
        listRoadCodeList();
        getFacilityList();
        getSceneByTypeReport();
    }])
    .service('analyzePutIF', ['$http', 'commonData', 'Tool', function($http, commonData) {
        var userIF = {
            getSceneByTypeReport: function (params) {
                return $http.get('/stat/scout-quatity-analyze/cost/getSceneByTypeReport', {
                    params: params
                });
            },
            getSceneDamageReport: function (params) {
                return $http.get('/stat/scout-quatity-analyze/cost/getSceneDamageReport', {
                    params: params
                });
            },
            getFacilityList: function () {
                return $http.get('/stat/scout-quatity-analyze/cost/getFacilityList');
            }
        };
        return angular.extend({}, commonData, userIF);
    }])
;


