require('../css/main.css');

require('./util/global.js');

// 内部模块
require('./config/global.config.js');
require('./modules.js');
require('./login.js');
require('./util/http.js');
require('./modules/common/common.js');

angular
    .module('app', [
        'ngRoute',
        'ui.bootstrap',
        'angularFileUpload',
        'http-3rd',
        'ui.select',
        'app.common',
        'app.config',
        'app.modules'
    ])
    .config(['$routeProvider', 'uibPaginationConfig', '$localeProvider', 'uibDatepickerConfig', 'uibDatepickerPopupConfig', function($routeProvider, uibPaginationConfig, $localeProvider, uibDatepickerConfig, uibDatepickerPopupConfig) {
        // $routeProvider.otherwise({
        //     redirectTo: '/manage-material'
        // });
        angular.extend(uibPaginationConfig, {
            'nextText': '›',
            'firstText': '«',
            'lastText': '»',
            'previousText': '‹',
            'maxSize': 5
        });
        angular.extend(uibDatepickerConfig, {
            formatDayTitle: 'yyyy-MM'
        });
        angular.extend(uibDatepickerPopupConfig, {
            'currentText': '今天',
            'clearText': '清除',
            'closeText': '关闭',
            'showButtonBar': true
        });
        //将日期的周几的英文表示改成中文表示
        //在angularjs文件的 $LocaleProvider 里可以查看具体情况
        var clone = $localeProvider.$get();
        $localeProvider.$get = function () {
            var $locale = {};
            angular.merge($locale, clone, {
                DATETIME_FORMATS: {
                    SHORTDAY: '日,一,二,三,四,五,六'.split(','),
                    MONTH: '一月,二月,三月,四月,五月,六月,七月,八月,九月,十月,十一月,十二月'
                        .split(',')
                }
            });
            return $locale;
        };
    }])
    .run(['$rootScope', '$route', 'global', 'Notify', 'Alert', function($rootScope, $route, global, Notify, Alert) {
        $rootScope.global = global;
        $rootScope.Notify = Notify;
        $rootScope.Alert = Alert;
        $rootScope.currentDate = window.APP_GLOBAL.getNowFormatDate();
        $rootScope.navs = [];
        $rootScope.isAdmin = window.T && window.T.currentUser.organization.organization_type_id == 1;
        $rootScope.organizationId = window.T && window.T.currentUser.organization_id || '';
        var normalNavs = [{
            "type": "analyze-destroy",
            "name": "典型损坏养护分析",
            "sub": [{
                type: 'analyze-destroy/1',
                name: '裂缝防治效果分析'
            }, {
                type: 'analyze-destroy/2',
                name: '路面变形防治效果分析'
            }]
        }, {
            "type": "analyze-cost",
            "name": "成本效益分析",
            "sub": [{
                type: 'analyze-human',
                name: '人力分包'
            }, {
                type: 'analyze-device',
                name: '机械耗用'
            }, {
                type: 'analyze-material',
                name: '材料耗费'
            }, {
                type: 'analyze-money',
                name: '费用分析'
            }]
        }, {
            "type": "analyze-quality-and-put",
            "name": "养护质量与投入分析",
            "sub": [{
                type: 'analyze-quality',
                name: '养护质量分析'
            }, {
                type: 'analyze-put',
                name: '养护投入分析'
            }]
        }];
        var adminNav = {
            "type": "daily",
            "name": "日志管理",
            "sub": [{
                type: 'daily/1',
                name: '登录日志'
            }, {
                type: 'daily/2',
                name: '操作日志'
            }]
        };
        $rootScope.siteInfo = window.T || {};
        if (window.T && window.T.nav) {
            $rootScope.navs =  window.T.nav.concat(normalNavs);
        }

        if ($rootScope.isAdmin) {
            $rootScope.navs.push(adminNav);
        }
        $rootScope.activeIndex = -1;
        //使用link-active,active-class属性， 看起来像指令但不是指令，你可以将链接激活时的class 放在active-class 属性里，默认是active
        $rootScope
            .$on('$routeChangeSuccess', function () {
                if (document.getElementById('form-sign')) {
                    return;
                }
                $rootScope.isEntry = false;
                // $rootScope.isEntry = true;
                var activeLinks = document.querySelectorAll('[link-active]');
                function checkLinkActive(state, itemLink) {
                    if (!state) {
                        return false; //首次进入获取不到state
                    }
                    if (state == itemLink || state.indexOf(itemLink) != -1) {
                        return true;
                    } else {
                        return false;
                    }
                }
                for (var i = 0, len = activeLinks.length; i < len; i++) {
                    var $this = angular.element(activeLinks[i]),
                        activeClass = 'active',
                        state = $route.current.state,
                        itemLink = $this.attr("link-active");
                    if (checkLinkActive(state, itemLink)) {
                        $this.parent().addClass(activeClass);
                        $rootScope.isEntry = true;
                    } else {
                        $this.parent().removeClass(activeClass);
                    }
                }
                // 最后的权限判断
                var pageTip = document.getElementById('page-tip');
                if (!pageTip) {
                    return;
                }
                if (!$rootScope.isEntry) {
                    document.getElementById('page-tip').style.display = 'block';
                } else {
                    document.getElementById('page-tip').style.display = 'none';
                }
            })
        ;

        $rootScope.toggleSub = function(index) {
            if (index == $rootScope.activeIndex) {
                $rootScope.activeIndex = -1;
            } else {
                $rootScope.activeIndex = index;
            }
        }

    }])
;

angular.element(document).ready(function () {
    angular.bootstrap(document, ["app"]);
});