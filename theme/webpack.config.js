var webpack = require('webpack');
var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var postcssImport = require('postcss-import');
var autoprefixer = require('autoprefixer');
var cssnested = require('postcss-nested');
var cssnano = require('cssnano');
var customProperties = require("postcss-custom-properties");

//指定资源的位置
var publicPath = '/theme';


module.exports = {
    entry: {
        lib: ['angular', 'angular-route', 'angular-file-upload', 'angular-ui-bootstrap', 'angular-bootstrap-datetimepicker', 'ui-select'],
        main: './src/js/app.js'
    },
    output: {
        path: "./",
        publicPath: publicPath,
        filename: "dist/js/[name].js"
    },
    postcss: function(webpack) {
        return [
            postcssImport({
                addDependencyTo: webpack
            }),
            cssnested(),
            customProperties(),
            autoprefixer({
                browsers: ['> 1%', 'Android 2.3', 'iOS 7']
            }),
        ];
    },
    module: {
        loaders: [{
            test: /\.(png|jpeg|jpg|gif|svg)$/,
            loaders: [
               'url?limit=10000&name=/dist/images/[name].[ext]?[hash:7]',
                'image-webpack?{bypassOnDebug:true,progressive:true, optimizationLevel: 7, interlaced: false, pngquant:{quality: "65-90", speed: 4}}'
            ]
        }, {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract('style-loader',
                'css-loader?importLoaders=1!postcss-loader!sass')
        }, {
            test: /\.tmpl/,
            loader: 'underscore-template-loader'
        }, {
            test: /[\/]angular\.js$/, loader: "exports?angular"
        }, {
            test: /\.html$/,
            loader: 'ngtemplate?relativeTo=' + (path.resolve(__dirname, './src/js/modules')) + '/!html'
        }, {
            test: /\.(woff|eot|ttf|woff2|svg)$/,
            loader: 'file-loader?name=dist/font/[name].[ext]?[hash:7]'
        }, {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
            query: {
                presets: ['es2015']
            }
        }]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "lib",
            minChunks: Infinity
        }),
        new ExtractTextPlugin("dist/css/main.css", {
            allChunks: true
        })
    ]
};
